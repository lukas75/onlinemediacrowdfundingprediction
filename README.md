# My project's README

## Crowdfunding Prediction Framework using Online Media
As part of the master thesis "Leveraging user generated online media for predicting crowdfunding success" this is a framework for a system, that automatically crawls data from online resources to feed a machine learning application with data to further make predictions and present ranked results on a web page. The prototype implementation is centered around crowdfunding and calculates novelty, popularity and successfulness from data originating from Twitter, online news platforms and Kickstarter. 
 
## Installation
how to get a development env / prototype running: 

deploy on Webserver and run server:port/OnlineMediaCrowdfundingPrediction/
for instance: http://localhost:8080/OnlineMediaCrowdfundingPrediction/

To start run the machine learning component the file called appModel.ipynb needs to be started. You can run it in JupyterLab. If you are not running the server on localhost:8080 you will have to change the variables "purl" and "url" in the file before you run it. 
The application relies on several dependencies. You can use the exported Anaconda environment file "environment.yml" to satisfy the dependencies. 

In the resources folder, there is a file called "index.txt". The path to the folder containing the lucene indexes has to be written down inside. 

To retrieve data from Twitter there needs to be a file in the main directory named twitterauth.txt
This must include the consumer key for the app in the first line and the consumer secret in the second line
In order to get those you have to go to the developer portal and create a twitter app see: 
https://developer.twitter.com/en/docs/basics/authentication/guides/access-tokens.html

 * if you start with an empty Lucene project index the following steps are necessary callable from within the run package as arguments in the java application: 
 * run "prerun" to add some project files to the index
 * run "scrapeKickstarter" to add some projects from Kickstarter to the index and analyze the scraped data
   to provide a training set for the machine learning component
 * run "updateProjects" to update the project after some time to be able to generate labels
 * run "storm" to scrape news web sites and add those to the news index for popularity prediction

## API Reference
 
[Contribution guidelines for this project](docs/index.html)

## Screenshots
![Alt text](UI_full.png?raw=true "GUI")
 
## Tech/framework used
<b>Built with</b>
- [Apache Lucene]()
- [Apache Stormcrawler]()
- [JSoup]()
- [Tensorflow]()
- [Keras]()
- [XGBoost]()

 
## Contribute
 
* Fork the repo on GitHub
* Clone the project to your own machine
* Commit changes to your own branch
* Push your work back up to your fork
* Submit a Pull request so that we can review your changes
  NOTE: Be sure to merge the latest from "upstream" before making a pull request!
  
## Extending the Framework
Model classes can be added to the framework as long as they implement the In- dexingObject and respectively extend parent class Project. This satisfies a condi- tion arising from the fact, that data is stored persistently in the index. Namely the condition, that an object can be created from the index through an appropriate constructor. This constructor should be overwritten of course when subclassing the Project class.
To collect data one has to write a reader class by extending the abstract class ModelReader provided in Listing 5.2.
Lastly the machine learning model has to be enhanced accordingly, in case new features should be integrated into the prediction model.
To utilize newly integrated model features it is necessary to adapt the machine learning model accordingly.