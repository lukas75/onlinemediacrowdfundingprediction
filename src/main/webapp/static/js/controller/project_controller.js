'use strict';

angular.module('myApp').controller('ProjectController', ['$scope', 'ProjectService', function($scope, ProjectService) {
    var self = this;
    self.project={name:'', platform:'', goal:''};
    //self.projects=[];

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.getIndexofProject = getIndexofProject; 
    self.isaDate = isaDate;
    self.isDate = angular.isDate;
    

    fetchAllProjects();

    function fetchAllProjects(){
    	ProjectService.fetchAllProjects()
            .then(
            function(d) {
                self.projects = d;
                console.error(d);
            },
            function(errResponse){
                console.error('Error while fetching Projects');
            }
        );
    }

    function createProject(project){
    	ProjectService.createProject(project)
            .then(
            //fetchAllProjects,
            function(errResponse){
                console.error('Error while creating Project');
            }
        );
    }

    function updateProject(project, id){
    	ProjectService.updateProject(project, id)
            .then(
            fetchAllProjects,
            function(errResponse){
                console.error('Error while updating Project');
            }
        );
    }

    function deleteProject(id){
    	ProjectService.deleteProject(id)
            .then(
            fetchAllProjects,
            function(errResponse){
                console.error('Error while deleting Project');
            }
        );
    }

    function submit() {
        //if(self.project.id===null){
            console.log('Saving New Project', self.project);
            createProject(self.project);
//        }else{
//            updateProject(self.project, self.project.id);
//            console.log('Project updated with id ', self.project.id);
//        }
        reset();
    }

    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.projects.length; i++){
            if(self.project[i].id === id) {
                self.project = angular.copy(self.project[i]);
                break;
            }
        }
    }

    function remove(id){
        console.log('id to be deleted', id);
        if(self.project.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteProject(id);
    }


    function reset(){
        self.project={name:'',platform:'',goal:''};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function isaDate(v) {
    	return angular.isDate(v); 
    }
    
    function getIndexofProject(pro) {
    return Object.keys(pro).indexOf('pid');
    }

}]);
