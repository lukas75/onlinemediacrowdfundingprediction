<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>  
    <title>Crowd Funding Platform</title>  
    <style>
      .name.ng-valid {
          background-color: lightgreen;
      }
      .name.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .username.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

      .platform.ng-valid {
          background-color: lightgreen;
      }
      .platform.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .platform.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link href="<c:url value='/static/css/app.css' />" rel="stylesheet"></link>
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="ProjectController as ctrl">
          <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Project Registration Form </span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.project.pid" />

                        
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Concept Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.project.conceptname" class="features form-control input-sm" placeholder="Enter concept name" required/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.features.$error.required">This is a required field</span>
                                      <span ng-show="myForm.features.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Platform</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.project.platform" class="form-control input-sm" placeholder="Enter CF Platform"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.features.$error.required">This is a required field</span>
                                      <span ng-show="myForm.features.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      

                       <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="file">Goal</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.project.score" class="features form-control input-sm" placeholder="Enter some goal" required/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.features.$error.required">This is a required field</span>
                                      <span ng-show="myForm.features.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div> 

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{'Create'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Projects </span></div>
              <div class="tablecontainer">
                  <table id="tbl" class="table table-hover">

                      <thead>
                          <tr  > 
                          <th ng-repeat="(key, value) in ctrl.projects[0]" ng-init="pindex = ctrl.getIndexofProject(ctrl.projects[0])" ng-show = "pindex <= $index">
                          <span ng-if = "pindex <= $index" ng-bind="key">{{key}}</span> </th>
                          </tr>
                      </thead>
                      <tbody ng-repeat="u in ctrl.projects" ng-init="pindex = ctrl.getIndexofProject(u)">
                          <tr>
                          	  <td ng-repeat="(key, value) in u" ng-show = "pindex <= $index">
                              	<span ng-if = "pindex <= $index" ng-bind="value">{{value}}</span>
                              </td>
                              <td>
<!--                          <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button> -->
                              <button type="button" ng-click="hideShow=(hideShow ? false : true)" class="btn btn-success custom-width">Details</button>
                              <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
                              </td>
                          </tr>
                          <tr ng-if="hideShow" ><th ng-repeat="(key, value) in u"  ng-show = "pindex > $index">
                             <span ng-if = "pindex >$index" ng-bind="key">{{key}}</span>
                          </th></tr>
                          <tr ng-if="hideShow"><td ng-repeat="(key, value) in u"  ng-show = "pindex > $index">
                             <span ng-if = "pindex > $index" ng-bind="value">{{value}}</span>
                          </td></tr>
                      </tbody>
                  </table>
                  

                  
              </div>
          </div>
      </div>
      
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
      <script src="<c:url value='/static/js/app.js' />"></script>
      <script src="<c:url value='/static/js/service/user_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/user_controller.js' />"></script>
      
      <script src="<c:url value='/static/js/service/project_service.js' />"></script>
      <script src="<c:url value='/static/js/controller/project_controller.js' />"></script>
  </body>
</html>