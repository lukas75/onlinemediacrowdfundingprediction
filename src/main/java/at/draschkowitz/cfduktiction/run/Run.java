package at.draschkowitz.cfduktiction.run;

import java.io.File;
import java.util.List;

import com.digitalpebble.stormcrawler.ConfigurableTopology;

import java.util.ArrayList;

import at.draschkowitz.cfduktiction.crawler.KickstarterReader;
import at.draschkowitz.cfduktiction.crawler.StormCrawler;
import at.draschkowitz.cfduktiction.crawler.TwitterReader;
import at.draschkowitz.cfduktiction.indexer.Indexer;
import at.draschkowitz.cfduktiction.model.IndiegogoProject;
import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;



/**
 * 
 * main methods
 * prerun has to be done manually before running the server for the first time 
 *possible arguments are: prerun, scrapeKickstarter, updateProjects, writeIndexToFile, getIndexSize, twittertest
 *
 * if you start with an empty Lucene project index the following steps are necesarry: 
 * 1. run "prerun" to add some project files to the index
 * 2. run scrapeKickstarter to add some projects from Kickstarter to the index and analyse the scraped data
 *    to provide a training set for the machine learning component
 * 3. update the project after some time to be able to generate labels
 * 4. run "storm" to scrape news web sites and add those to the news index for popularity prediction
 *
 */

public class Run {
	
	//read in projects from file to start of ? 
	//create pretraining set with analyzed projects contain popstat, simstat and label 
	//bzw with künstlichen startscore ? dont know wie ich das gmacht hab 

	public static void main(String[] args) {
		String arg = args[0]; 
		switch (arg) {
		case "twittertest": twittertest(); break; 
		case "writeIndexToFile": writeIndexToFile(); break; 
		case "updateProjects": updateProjects(); break; 
		case "prerun": prerun(); break; 
		case "scrapeKickstarter": scrapeKickstarter(); break; 
		case "getIndexSize": getIndexSize(); break; 
		case "storm": storm(); break; 
		default: System.out.println("Command not found"); break; 
		}
			

	}
	
    public static void storm() {
    	try {
    	ConfigurableTopology.start(new StormCrawler(), new String[]{"-conf crawlerconf.yaml","-local"} );
    	} catch (Exception e) {e.printStackTrace();}
    }
	
	//test Twitter connection 
	public static void twittertest(){
		TwitterReader tr = TwitterReader.getInstance(); 
		tr.getAllTweets("crowdfunding new"); 
	}
	
	//write lucene index into csv file 
	public static void writeIndexToFile(){
		Indexer i = new Indexer(); 
		i.setIndex("project");
		i.writenewDocsToFile("2608_newjson"); 
	}

  //update pledged amount of projects 
  public static void updateProjects() {
  	KickstarterReader bwc = new KickstarterReader();
  	bwc.updateProjectsNew(); 
  }
  
  //prerun projects import from files without data analysis or score prediction
  	@SuppressWarnings("unchecked")
  	public static void prerun() {
	  Indexer in = new Indexer(); 
	  in.setIndex("project");
  
	  List<Project>l = in.readProjectsFromFile(
			new File("/Users/lukasdraschkowitz/Downloads/Indiegogo_2018-02-16T10_41_02_245Z.json")
			, IndiegogoProject.class); 
			  //cast list<Project> to List<Indiegogoproject>
			  in.index((List<IndiegogoProject>)(List<?>) l);
			  
//	  List<Project> l1 = in.readProjectsFromFile(
//						new File("/Users/lukasdraschkowitz/Downloads/Kickstarter_2018-02-15T03_20_44_743Z.json")
//						, KickstarterProject.class); 
//						  //cast list<Project> to List<Kickstarterproject>
//						  in.index((List<KickstarterProject>)(List<?>) l1);	  
	}
  	
  	//retrieve and index projects from Kickstarter
  	public static void scrapeKickstarter() {
  		KickstarterReader bwc = new KickstarterReader();
  		Indexer in = new Indexer(); 
  		List<Project> l = new ArrayList<Project>(); 
  		bwc.getPageLinks("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1",2); 
  		bwc.retrieveProjects(l); 
  		System.out.println(l.size()); 
  		List<KickstarterProject> l1 = new ArrayList<KickstarterProject> (); 
  		for (Project p : l) {l1.add((KickstarterProject) p); } 
  		in.index(l1); 
}

  	//print index size
  	public static void getIndexSize(){
  		Indexer in = new Indexer(); 
  		in.setIndex("project");
  		//System.out.println("# docs in news index: "+in.searchAllDocs().size());	 
  	}
  	
}   