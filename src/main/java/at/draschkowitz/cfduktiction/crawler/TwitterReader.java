package at.draschkowitz.cfduktiction.crawler;

import twitter4j.*;
import twitter4j.auth.OAuth2Token;
import twitter4j.conf.ConfigurationBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import at.draschkowitz.cfduktiction.model.TwitterFeatures;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;

import edu.stanford.nlp.util.logging.RedwoodConfiguration;

/**
 *reads from Twitter
 *450 calls per 15 minute window
 *100 tweets per call
 *
 *
 *sentiment analyzer from stanfordnlp analyzes sentiment for individual tweets
 *
 *consumer_key and consumer_secret will be read from a file called .. that needs to be in resources folder
 *
 *singleton class but not multi-thread ready, can't be subclassed as constructor is private
 *
 *parts of the code from 
 *http://www.socialseer.com/twitter-programming-in-java-with-twitter4j/how-to-retrieve-more-than-100-tweets-with-the-twitter-api-and-twitter4j/
 *uses application authentication
 */
public class TwitterReader {

	//	Set this to your actual CONSUMER KEY and SECRET for your application as given to you by dev.twitter.com
	private static String CONSUMER_KEY; 
	private static String CONSUMER_SECRET; 

	//	How many tweets to retrieve in every call to Twitter. 100 is the maximum allowed in the API
	private static final int TWEETS_PER_QUERY		= 100;

	//	This controls how many queries, maximum, we will make of Twitter before cutting off the results.
	//	You will retrieve up to MAX_QUERIES*TWEETS_PER_QUERY tweets.
	//
	//  If you set MAX_QUERIES high enough (e.g., over 450), you will undoubtedly hit your rate limits
	//  and you can see the program sleep until the rate limits reset
	private static final int MAX_QUERIES			= 10; //consequently no more than 1000 tweets per query are returned
	
	//stanfordnlp to compute sentiment
	static StanfordCoreNLP pipeline;
	
	static Twitter twitter; 
	static File file;
	
	//singleton class but not multi-thread ready, can't be subclassed as constructor is private
	private static TwitterReader instance = null;
	
	private TwitterReader() {
		init();
		twitter = getTwitter(); 
//		file = new File("resources/twitterauth.txt"); 
	}
	
	public static TwitterReader getInstance() {
		   if(instance == null) {
		         instance = new TwitterReader();
		      }
		   return instance;
	}
	
	public static void getKeyAndSecretfromFile() throws IOException {
				ClassLoader classLoader = TwitterReader.class.getClassLoader();
				file = new File(classLoader.getResource("twitterauth.txt").getFile());
				System.out.println(file.toURI()); 
	            BufferedReader b = new BufferedReader(new FileReader(file));
	            CONSUMER_KEY = b.readLine();
	            CONSUMER_SECRET = b.readLine();
	            b.close();
	}
	  
	
	/**
	 * method to compute twitter score
	 * Function that describes TwitterScore = (log(Cr) ∗ 15 + log(Cf) ∗ 10 + log( 1/Tl )) ∗ f(S)
	 * number of retweets (Cr), number of times the tweet got favorited (Cf), the length of the tweet (Tl)
	 * f(S) = Function that computes Sentiment score for a Tweet: f(x) = 1/4*x*(x+1)
	 * @param tweets - List of Tweets 
	 * @return score, avgText, avgSent, totRetweet, totFav
	 */
	public static double[] computeScore (List<TwitterFeatures> tweets) {
		double re[] = new double[5];
		double score; 
		double s = 0; 
		double avgText = 0; 
		double avgSent = 0; 
		double totRetweet = 0; double totFav = 0; 
		int l = tweets.size(); 
		for (TwitterFeatures tweet : tweets) {
			score = 0; 
			avgText += tweet.getText().length();
			avgSent += tweet.getSentiment();
			totRetweet *= tweet.getRetweetCount(); 
			totFav *=tweet.getFavoriteCount(); 
			if (tweet.getRetweetCount()>0) {score+=Math.log10(tweet.getRetweetCount()*15);}
			if (tweet.getFavoriteCount()>0) {score+=Math.log10(tweet.getFavoriteCount()*10);}
			if (1/tweet.getText().length()>0) {score+=Math.log10(1/tweet.getText().length()); }
			if (tweet.getSentiment()>0) {score*=Math.log10(1/5*tweet.getSentiment()*(tweet.getSentiment()+1)); }
			s+=score; 
		}
		avgText /= l; 
		avgSent /= l; 
		re[0] = s; re[1] = avgText; re[2] = avgSent; re[3] = totRetweet; re[4] = totFav; 
		return re; 
	}
	
	/**
	 * initialize StanfordCoreNLP pipeline to analyze Tweet text
	 */
	public static void init() {
		RedwoodConfiguration.current().clear().apply(); //disable logs
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, pos, parse, sentiment");   
		//The default Stanford PCFG parser is cubic time complexity with respect to the sentence length. 
		//This is why we usually recommend restricting the maximum sentence length for performance reasons
		props.setProperty("parse.maxlen", "20"); 
		//props.setProperty("tokenize.options", "untokenizable=noneDelete");
		
		/*faster parser 
		Use Shift-Reduce Constituency Parsing (O(n),
		http://nlp.stanford.edu/software/srparser.shtml) vs CoreNLP's default
		Probabilistic Context-Free Grammar Parsing (O(n^3))
		*/
		//props.setProperty("parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz");

		pipeline = new StanfordCoreNLP(props);
		
	}
	
	/**
	 * computes sentiment score for a tweet according to StanfordCoreNLP
	 * between 0, signaling very negative tweets, and 4, signaling very positive tweets
	 * a value of 2 describes neutral tweets and -1 is assigned to unclassifiable tweets
	 * @param tweet
	 * @return sentiment for a tweet
	 */
	public static int findSentiment(String tweet) {
		tweet = tweet.replaceAll("[\\uD83D\\uFE0F]", "");
		int mainSentiment = 0;
		if (tweet != null && tweet.length() > 0) {
			int longest = 0;
			Annotation annotation = pipeline.process(tweet);
			for (CoreMap sentence : annotation
					.get(CoreAnnotations.SentencesAnnotation.class)) {
				Tree tree = sentence
						.get(SentimentAnnotatedTree.class);
				int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
				String partText = sentence.toString();
				if (partText.length() > longest) {
					mainSentiment = sentiment;
					longest = partText.length();
				}

			}
		}
		//System.out.println(mainSentiment+"sent"); 
		return mainSentiment;
	}	


	/**
	 * Replace newlines and tabs in text with escaped versions to making printing cleaner
	 *
	 * @param text	The text of a tweet, sometimes with embedded newlines and tabs
	 * @return		The text passed in, but with the newlines and tabs replaced
	 */
	public static String cleanText(String text)
	{
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");

		return text;
	}


	/**
	 * Retrieve the "bearer" token from Twitter in order to make application-authenticated calls.
	 *
	 * This is the first step in doing application authentication, as described in Twitter's documentation at
	 * https://dev.twitter.com/docs/auth/application-only-auth
	 *
	 * Note that if there's an error in this process, we just print a message and quit.  That's a pretty
	 * dramatic side effect, and a better implementation would pass an error back up the line...
	 *
	 * @return	The oAuth2 bearer token
	 */
	public static OAuth2Token getOAuth2Token()
	{
		OAuth2Token token = null;
		ConfigurationBuilder cb;

		cb = new ConfigurationBuilder();
		cb.setApplicationOnlyAuthEnabled(true);
		
		try {
			TwitterReader.getKeyAndSecretfromFile(); 	
			cb.setOAuthConsumerKey(CONSUMER_KEY).setOAuthConsumerSecret(CONSUMER_SECRET);

			token = new TwitterFactory(cb.build()).getInstance().getOAuth2Token();
		}
		catch (Exception e)
		{
			System.out.println("Could not get OAuth2 token");
			e.printStackTrace();
			System.exit(0);
		}

		return token;
	}

	/**
	 * Get a fully application-authenticated Twitter object useful for making subsequent calls.
	 *
	 * @return	Twitter4J Twitter object that's ready for API calls
	 */
	public static Twitter getTwitter()
	{
		OAuth2Token token;

		//	First step, get a "bearer" token that can be used for our requests
		token = getOAuth2Token();

		//	Now, configure our new Twitter object to use application authentication and provide it with
		//	our CONSUMER key and secret and the bearer token we got back from Twitter
		ConfigurationBuilder cb = new ConfigurationBuilder();

		cb.setApplicationOnlyAuthEnabled(true);

		cb.setOAuthConsumerKey(CONSUMER_KEY);
		cb.setOAuthConsumerSecret(CONSUMER_SECRET);

		cb.setOAuth2TokenType(token.getTokenType());
		cb.setOAuth2AccessToken(token.getAccessToken());

		//	And create the Twitter object!
		return new TwitterFactory(cb.build()).getInstance();

	}

	/**
	 * searches for a keyword on twitter and retrieves relevant Tweets
	 * is set to make maximum MAX_QUERIES calls per query 
	 * results in max 100* MAX_QUERIES tweets per query string
	 * MAX_QUERIES is currently set to 10 which results in 1000 tweets per query
	 * @param keyword
	 * @return List of Tweets 
	 */
	public synchronized List<TwitterFeatures> getAllTweets(String keyword)
	{
		System.out.println("getting Tweets"); 
		List<TwitterFeatures> returnlist = new ArrayList<TwitterFeatures>() ;
		
		//	We're curious how many tweets, in total, we've retrieved.  Note that TWEETS_PER_QUERY is an upper limit,
		//	but Twitter can and often will retrieve far fewer tweets
		int	totalTweets = 0;

		//	This variable is the key to our retrieving multiple blocks of tweets.  In each batch of tweets we retrieve,
		//	we use this variable to remember the LOWEST tweet ID.  Tweet IDs are (java) longs, and they are roughly
		//	sequential over time.  Without setting the MaxId in the query, Twitter will always retrieve the most
		//	recent tweets.  Thus, to retrieve a second (or third or ...) batch of Tweets, we need to set the Max Id
		//	in the query to be one less than the lowest Tweet ID we've seen already.  This allows us to page backwards
		//	through time to retrieve additional blocks of tweets
		long maxID = -1;

		//	Now do a simple search to show that the tokens work
		try
		{
			//	There are limits on how fast you can make API calls to Twitter, and if you have hit your limit
			//	and continue to make calls Twitter will get annoyed with you.  I've found that going past your
			//	limits now and then doesn't seem to be problematic, but if you have a program that keeps banging
			//	the API when you're not allowed you will eventually get shut down.
			//
			//	Thus, the proper thing to do is always check your limits BEFORE making a call, and if you have
			//	hit your limits sleeping until you are allowed to make calls again.
			//
			//	Every time you call the Twitter API, it tells you how many calls you have left, so you don't have
			//	to ask about the next call.  But before the first call, we need to find out whether we're already
			//	at our limit.

			//	This returns all the various rate limits in effect for us with the Twitter API
			Map<String, RateLimitStatus> rateLimitStatus = twitter.getRateLimitStatus("search");

			//	This finds the rate limit specifically for doing the search API call we use in this program
			RateLimitStatus searchTweetsRateLimit = rateLimitStatus.get("/search/tweets");


			//	Always nice to see these things when debugging code...
			/*System.out.printf("You have %d calls remaining out of %d, Limit resets in %d seconds\n",
							  searchTweetsRateLimit.getRemaining(),
							  searchTweetsRateLimit.getLimit(),
							  searchTweetsRateLimit.getSecondsUntilReset());
			// */

			//	This is the loop that retrieve multiple blocks of tweets from Twitter
			for (int queryNumber=0;queryNumber < MAX_QUERIES; queryNumber++)
			{
				//System.out.printf("\n\n!!! Starting loop %d\n\n", queryNumber);

				//	Do we need to delay because we've already hit our rate limits?
				if (searchTweetsRateLimit.getRemaining() == 0)
				{
					//	Yes we do, unfortunately ...
					System.out.printf("!!! Sleeping for %d seconds due to rate limits\n", searchTweetsRateLimit.getSecondsUntilReset());

					//	If you sleep exactly the number of seconds, you can make your query a bit too early
					//	and still get an error for exceeding rate limitations
					//
					// 	Adding two seconds seems to do the trick. Sadly, even just adding one second still triggers a
					//	rate limit exception more often than not.  I have no idea why, and I know from a Comp Sci
					//	standpoint this is really bad, but just add in 2 seconds and go about your business.  Or else.
					Thread.sleep((searchTweetsRateLimit.getSecondsUntilReset()+2) * 1000l);
				}

				Query q = new Query(keyword);			    // Search for tweets that contains this term
				q.setCount(TWEETS_PER_QUERY);				// How many tweets, max, to retrieve
				q.getResultType();						    // Get all tweets
				q.setLang("en");							// English language tweets

				//	If maxID is -1, then this is our first call and we do not want to tell Twitter what the maximum
				//	tweet id is we want to retrieve.  But if it is not -1, then it represents the lowest tweet ID
				//	we've seen, so we want to start at it-1 (if we start at maxID, we would see the lowest tweet
				//	a second time...
				if (maxID != -1)
				{
					q.setMaxId(maxID - 1);
				}

				//	This actually does the search on Twitter and makes the call across the network
				QueryResult r = twitter.search(q);

				//	If there are NO tweets in the result set, it is Twitter's way of telling us that there are no
				//	more tweets to be retrieved.  Remember that Twitter's search index only contains about a week's
				//	worth of tweets, and uncommon search terms can run out of week before they run out of tweets
				if (r.getTweets().size() == 0)
				{
					break;			// Nothing? We must be done
				}


				//	loop through all the tweets and process them.  In this sample program, we just print them
				//	out, but in a real application you might save them to a database, a CSV file, do some
				//	analysis on them, whatever...
				for (Status s: r.getTweets())				// Loop through all the tweets...
				{
					//	Increment our count of tweets retrieved
					totalTweets++;

					//	Keep track of the lowest tweet ID.  If you do not do this, you cannot retrieve multiple
					//	blocks of tweets...
					if (maxID == -1 || s.getId() < maxID)
					{
						maxID = s.getId();
					}

					//	Do something with the tweet....
					/*
					 * output tweet	
					System.out.printf("Sent: %s - At %s, @%-20s said:  %s\n",    
									  Integer.toString(findSentiment(cleanText(s.getText()))),
									  s.getCreatedAt().toString(),
									  s.getUser().getScreenName(),
									  cleanText(s.getText())
									  );
					 */
					//just add the tweet to return list
					returnlist.add( new TwitterFeatures(s, findSentiment(cleanText(s.getText()))) ); 
				}

				//	As part of what gets returned from Twitter when we make the search API call, we get an updated
				//	status on rate limits.  We save this now so at the top of the loop we can decide whether we need
				//	to sleep or not before making the next call.
				searchTweetsRateLimit = r.getRateLimitStatus();
			}

		}
		catch (Exception e)
		{
			//	Catch all -- you're going to read the stack trace and figure out what needs to be done to fix it
			System.out.println("That didn't work well...wonder why?");

			e.printStackTrace();

		}

		System.out.printf("\n\nA total of %d tweets retrieved\n", totalTweets);
		//	That's all, folks!
		return returnlist; 
	}
	
	
}