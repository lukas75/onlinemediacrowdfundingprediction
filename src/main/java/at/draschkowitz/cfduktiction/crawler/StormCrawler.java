package at.draschkowitz.cfduktiction.crawler;

import com.digitalpebble.stormcrawler.ConfigurableTopology;
import com.digitalpebble.stormcrawler.Constants;

/**
 * Licensed to DigitalPebble Ltd under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * DigitalPebble licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.digitalpebble.stormcrawler.bolt.FetcherBolt;
import com.digitalpebble.stormcrawler.bolt.SiteMapParserBolt;
import com.digitalpebble.stormcrawler.bolt.URLPartitionerBolt;
import com.digitalpebble.stormcrawler.persistence.MemoryStatusUpdater;
import com.digitalpebble.stormcrawler.spout.MemorySpout;

import at.draschkowitz.cfduktiction.indexer.MyStdOutIndexer;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.storm.topology.TopologyBuilder;
import org.apache.storm.tuple.Fields;

/**
 * configuration of StormCrawler - ready to crawl news sites
 * provides function to run
 */
public class StormCrawler extends ConfigurableTopology {

    @Override
    protected int run(String[] args) {
        TopologyBuilder builder = new TopologyBuilder();
        
        String[] testURLs = readLines("news_sites.txt"); 

        builder.setSpout("spout", new MemorySpout(testURLs));
        builder.setBolt("partitioner", new URLPartitionerBolt()).shuffleGrouping("spout");
        builder.setBolt("fetch", new FetcherBolt()).fieldsGrouping("partitioner", new Fields("key"));
        builder.setBolt("sitemap", new SiteMapParserBolt()).localOrShuffleGrouping("fetch");
        //builder.setBolt("parse", new JSoupParserBolt()).localOrShuffleGrouping("sitemap");
        builder.setBolt("parse", new MyJsoupParserBolt()).localOrShuffleGrouping("sitemap");
        //builder.setBolt("tika", new ParserBolt()).localOrShuffleGrouping("shunt", "tika");
        builder.setBolt("index", new MyStdOutIndexer()).localOrShuffleGrouping("parse");

        Fields furl = new Fields("url");
        //Fields furl1 = new Fields("url", "content", "metadata");
        
        builder.setBolt("status", new MemoryStatusUpdater())
                .fieldsGrouping("fetch", Constants.StatusStreamName, furl)
                .fieldsGrouping("sitemap", Constants.StatusStreamName, furl)
                .fieldsGrouping("parse", Constants.StatusStreamName, furl)
                .fieldsGrouping("index", Constants.StatusStreamName, furl);

        conf.put("http.agent.name", "crowdfunding thesis"); 
        conf.put("fetcher.max.crawl.delay", "-1"); 
        //conf.put("fetchInterval.default","-1");
        //conf.put("urlfilters.config.ignoreOutsideHost", "true"); 
        //conf.put("urlfilters.ignoreOutsideHost", "true");
        //conf.put("max.depth", 0); 
        //conf.put("metadata.track.depth","true"); 
        conf.put("urlfilters.config.file", "stormfilters.json"); 
        return submit("crawl", conf, builder);
    }
    
    /**
     * reads in urls to crawl from configuration file newssites.txt
     * @param filename
     * @return
     */
    public String[] readLines(String filename)   {
        FileReader fileReader;
        List<String> lines = new ArrayList<String>();
		try {
			fileReader = new FileReader(filename);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line = null;
        while ((line = bufferedReader.readLine()) != null) 
        {
            lines.add(line);
        }
        bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        return lines.toArray(new String[lines.size()]);
    } 
    
}