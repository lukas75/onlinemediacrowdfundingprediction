package at.draschkowitz.cfduktiction.crawler;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.draschkowitz.cfduktiction.aggregator.PopularityPrediction;
import at.draschkowitz.cfduktiction.aggregator.SimilarityPrediction;
import at.draschkowitz.cfduktiction.indexer.Indexer;
import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;

import org.apache.commons.io.IOUtils;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import crawlercommons.robots.BaseRobotRules;
import crawlercommons.robots.SimpleRobotRulesParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * parts taken from tutorial at https://www.mkyong.com/java/jsoup-basic-web-crawler-example/
 * Class provides functions to read from kickstarter.com 
 */
public class KickstarterReader {
    private HashSet<String> links;    
	private BaseRobotRules rules; 


	public KickstarterReader() {
        links = new HashSet<>();
        //int max_crawlDelay = 1000; 

        //create rules from robots.txt file
        URL url = null;
		try {
			url = new URL("https://www.kickstarter.com/robots.txt");
		} catch (MalformedURLException e1) {e1.printStackTrace();}
        InputStream is = null; byte[] bytes = null;
        try {is = url.openStream ();
         bytes = IOUtils.toByteArray(is);
        } catch (IOException e) {} finally {if (is != null) try {is.close();} catch (IOException e) {e.printStackTrace();}}

        SimpleRobotRulesParser robotParser = new SimpleRobotRulesParser();
        rules = robotParser.parseContent("  sdsd ", bytes, "text/plain", "Any-darn-crawler");
    	System.out.println("1"+!rules.isAllowed("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1")); 
    }

	//	get links to project data from all projects - looping from page 1 to 11 here 
	//max numPages = 99
    public void getPageLinks(String URL, int numPages) { 
            try {
            	if (numPages >99) {numPages =99;}
            	if (numPages <2) {numPages = 2; }
            	for (int i = 1; i<1+numPages; i++) { //max numPages = <100
            		// possible to filter ... &woe_id, &category_id,...
	                Document document = Jsoup.connect("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page="+i).get();
	                document.body();  //   project-title
	                Elements otherLinks = document.getElementsByClass("js-react-proj-card col-full col-sm-12-24 col-lg-8-24");//"project-thumbnail-wrap"); 
	                for (Element page : otherLinks) {
	                	//String project_url = "https://www.kickstarter.com/"+page.select("a[href]").attr("href"); 
	                	String project = page.attr("data-project");
	                	String project_url =""; 
				        //"project":"https://www.kickstarter.com/projects/\\S+"
	                	//find link to article data
	                	Pattern p = Pattern.compile("\"project\":\"https://www.kickstarter.com/projects/\\S+\",");
				        Matcher m = p.matcher(project);
				        if (m.find()) {
				        	project_url = project.substring(m.start()+11 , m.end()-2); 
				        }
	                	if (links.add(project_url)) {
	                        //Remove the comment from the line below if you want to see it running on your editor
	                        System.out.println("url "+project_url);
	                    }
	                }
            	}
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
    }
    
    //Connect to each link saved in the article and find all the articles in the page
    //retrieves json data from kickstarter api
    public void retrieveProjects(final List<Project> l) {
		  Indexer in = new Indexer(); 
		  in.setIndex("project");
		  links.forEach(new Consumer<String>() {
			@Override
			public void accept(String x) {
			    Document document;
			    try {
			    	//catch rate limit reponse code and timeout
			    	Response response; 
			    	for (response = Jsoup.connect(x).execute(); response.statusCode() == 429; response = Jsoup.connect(x).execute()) {
			    		Thread.sleep(10000);
				        System.out.println("rate limit: cf");
			    	}
			        document = response.parse(); 
			        Elements a = document.select("div[class=\"mb3 mb10-sm mb3 js-risks\"]"); 
			        StringBuilder chall = new StringBuilder();
			        for (Element e : a) chall.append(e.text()); 
			        Elements b = document.select("div[class=\"full-description js-full-description responsive-media formatted-lists\"]"); 
			        StringBuilder desc = new StringBuilder();
			        for (Element e : b) if (desc.indexOf(e.text()) == -1) { desc.append(e.text()); }
			        String html = document.data();
			        Pattern p = Pattern.compile("https://api.kickstarter.com/v1/projects/\\S+?signature=\\S+?quot");
			        Matcher m = p.matcher(html);
			        String articleLink = ""; 
			        if (m.find()) {
			           articleLink = html.substring(m.start() , m.end()); 
			        }
			        ObjectMapper mapper = new ObjectMapper();
			        try {
						KickstarterProject project = mapper.readValue(new URL(articleLink), KickstarterProject.class);						
						
						Indexer in = new Indexer(); 
						in.setIndex("project");
						if (!in.pExists(project.getName())) {
						
						project.setDescription(desc.toString());
						project.setChallenges(chall.toString());
						project.setArticleLink(articleLink); 
						project.setArticlePage(x);

						//compute similarity to other projects regarding description field
						System.out.println("Name"+project.getName());
						double simScore = SimilarityPrediction.computeSimilarity(project.getName()); //getdescription
						double popScore = 0; 
						double[] pop = PopularityPrediction.computePopularity(project.getName(),project.getName()); //getBlurb
						popScore = pop[0]; 
						
						project.setSimScore(simScore);
						project.setPopScore(popScore);
						int score =  (int) (simScore+popScore); 
						project.setScore(score);
						System.out.println("sim: "+ simScore + "pop: "+ popScore); 
						project.setTwitterStat((int)pop[1]);
						project.setNewsStat((int)pop[2]);
						
						project.setAvgText(String.valueOf(pop[3]));
						project.setAvgSent(String.valueOf(pop[4]));
						project.setTotRetweet(String.valueOf(pop[5]));
						project.setTotFav(String.valueOf(pop[6]));
						
						l.add(project); 						
						}
			        } catch (IOException e) {System.err.println(e.getMessage());}    
			        
			    } catch (IOException e) {
			        System.err.println(e.getMessage());
			    } catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		});
        
        links.clear();
    }
    

    

    //read kickstarter projects from json file 
	public List<Project> readProjectsFromFile(File file, Class cl) {
    	List<Project> list = new ArrayList<Project>();
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
        	// Open the file
        	FileInputStream fstream = new FileInputStream(file);
        	BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        	String strLine;
        	//Read File Line By Line
        	//int b = 0; 
        	while (( (strLine = br.readLine()) != null ))   { // %% b<5000
        		JsonNode jsonNode = mapper.readTree(strLine);
        		JsonNode idNode = jsonNode.path("data"); 
        		KickstarterProject project = mapper.readValue(idNode.toString(), cl); 
        		list.add(project); //b++;
        	}
        	//Close the input stream
        	br.close(); 
        } catch (IOException e) {System.err.println(e.getMessage());}       
        return list; 
    }
    
    //run through all projects to update pledged
    public void updateProjectsNew() {
    	String newPledged =""; 
    	int size = 0; 
		  Indexer in = new Indexer(); 
		  in.setIndex("project");
    	List<org.apache.lucene.document.Document> pro = in.getPreTrainingDocuments();
		  //org.apache.lucene.document.Document d = in.search("", "title"); 
    	for (org.apache.lucene.document.Document d : pro) { 
    		String link = (d.get("articleLink"));
    		String page = d.get("articlePage"); 
    		if (page != null && !page.equalsIgnoreCase(link)) {
    		try {	
    		Document document = Jsoup.connect(page).get();
            Elements e = document.body().getElementsByClass("ksr-green-700");  //   project-title
            if (e != null && e.first() != null && e.first().text() != null ) {
            newPledged = e.first().text(); 
            Double newValue = 0.0;          
            newValue = Double.parseDouble(newPledged.replaceAll("[^0-9]", "")); 
    		Double p = 0.0; //initial p
    		if (d.get("pledgedInitial") == null) {p = Double.valueOf(d.get("pledged")); } else {
    	    if (Double.parseDouble(d.get("pledgedInitial")) == -1.0) {
    	    	p = Double.valueOf(d.get("pledged")); 
    	    } else {p = Double.parseDouble(d.get("pledgedInitial")); } }
    	    d.removeFields("pledgedInitial");
    	    d.removeFields("pledged");
    	    d.add(new StringField("pledgedInitial", String.valueOf(p), Field.Store.YES));
    	    d.add(new StringField("pledged", String.valueOf(newValue), Field.Store.YES));
    	    in.index(d);
    	    System.out.println("pledgedInitial: "+d.get("pledgedInitial")+":: "+d.get("pledged"));
    			}
    		} catch (IOException  e1) {System.out.println("error: "+e1); e1.printStackTrace();}
   	}
    	}
    
    	System.out.println("anzahl docs: "+size); 
    }


    
}


/* not working any longer
 * //    //run thorugh all projects to update pledged
//    public void updateProjects() {
//    	int size = 0; 
//		  Indexer in = new Indexer(); 
//		  in.setIndex("project");
//    	List<org.apache.lucene.document.Document> pro = in.getPreTrainingDocuments();
//    	for (org.apache.lucene.document.Document d : pro) { 
//    		String link = (d.get("articleLink"));
//    		String page = d.get("articlePage"); 
//    		if (page != null && !page.equalsIgnoreCase(link)) {
//    			size++; 
//    			if (size >10) {
//    			System.out.println("doc: "+link+" ---- "+page);
//    		try { Jsoup.connect(page).execute(); //.ignoreContentType(true)
//			} catch (IOException e1) {System.out.println("error: "+e1); e1.printStackTrace();}
//    		System.out.println(page+" "+d.get("title"));
//    		
//    		Double p = 0.0; //initial p
//    		if (d.get("pledgedInitial") == null) {p = Double.valueOf(d.get("pledged")); } else {
//    	    if (Double.parseDouble(d.get("pledgedInitial")) == -1.0) {
//    	    	p = Double.valueOf(d.get("pledged")); 
//    	    } else {p = Double.parseDouble(d.get("pledgedInitial")); } }
////    		take project and only change score and pledged
//    	    KickstarterProject k = null; 
//    	    ObjectMapper mapper = new ObjectMapper();
//    	    try {
//				 k = mapper.readValue(new URL(link), KickstarterProject.class);												
//	        } catch (IOException e) {System.err.println(e.getMessage());} 
//    	    d.removeFields("pledgedInitial");
//    	    d.removeFields("pledged");
//    	    d.add(new StringField("pledgedInitial", String.valueOf(p), Field.Store.YES));
//    	    d.add(new StringField("pledged", String.valueOf(k.getPledged()), Field.Store.YES));
//    	    in.index(d);
//    	    System.out.println("pledgedInitial: "+d.get("pledgedInitial")+":: "+d.get("pledged"));
//    		}	}
//    	}
//    	System.out.println("anzahl docs: "+size); 
//    }
 * 
 * 
 * also no longer working
 * // // da jetzt von der project page den api link holen anstatt zu parsen !  
//    //Connect to each link saved in the article and find all the articles in the page
//    public KickstarterProject retrieveProject(String x) {
//    			System.out.println(x); 
//			    Document document;
//			    KickstarterProject project = null; 
//			    try {
//			    	//catch rate limit reponse code and timeout
//			    	Response response; 
//			    	for (response = Jsoup.connect(x).ignoreContentType(true).execute(); response.statusCode() == 429; response = Jsoup.connect(x).ignoreContentType(true).execute()) {
//			    		Thread.sleep(10000);
//				        System.out.println("rate limit: cf");
//			    	}
//			    	System.out.println("response da"); 
//			        document = response.parse();
//			        System.out.println(document.data()); 
//			        //document = Jsoup.connect(x).get();
//
//	                //https://api.kickstarter.com/v1/projects/zahl?signature=zahlquot
//	                	//dann gibts comments, verschiedene reward, users, und eben signature für projects 
//			        //Elements linksOnPage = document.select("a[href]");   
//			        Elements a = document.select("div[class=\"mb3 mb10-sm mb3 js-risks\"]"); 
//			        StringBuilder chall = new StringBuilder();
//			        for (Element e : a) chall.append(e.text()); 
//			        //System.out.println("chall: "+ chall.toString());
//			        Elements b = document.select("div[class=\"full-description js-full-description responsive-media formatted-lists\"]"); 
//			        StringBuilder desc = new StringBuilder();
//			        for (Element e : b) desc.append(e.text()); 
//			        //System.out.println("desc: "+ desc.toString());
//			        String html = document.data();
//			        Pattern p = Pattern.compile("https://api.kickstarter.com/v1/projects/\\S+?signature=\\S+?quot");
//			        Matcher m = p.matcher(html);
//			        String articleLink = ""; 
//			        if (m.find()) {
//			           articleLink = html.substring(m.start() , m.end()); 
//			        }
//			        System.out.println(articleLink); 
//			        ObjectMapper mapper = new ObjectMapper();
//			        try {
//						 project = mapper.readValue(new URL(articleLink), KickstarterProject.class);												
//			        } catch (IOException e) {System.err.println(e.getMessage());}    
//			        
//			    } catch (IOException e) {
//			        System.err.println("error: "+e.getMessage());
//			    } catch (InterruptedException e1) {
//					e1.printStackTrace();System.err.println("error1: "+e1.getMessage());
//				}
//			    return project; 
//			
//    }
 */

