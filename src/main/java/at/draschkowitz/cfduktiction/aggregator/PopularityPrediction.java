package at.draschkowitz.cfduktiction.aggregator;

import java.util.List;
import java.io.File; 

import at.draschkowitz.cfduktiction.crawler.TwitterReader;
import at.draschkowitz.cfduktiction.indexer.Indexer;
import at.draschkowitz.cfduktiction.model.TwitterFeatures;

/**
 * workflow to compute popularity for project:
 * popularity is calculated by calling twitter for the CF project 
 * (plus calling Facebook index for information on CF project)
 * plus calling news information from news index for the CF project 
 * calculate score according to model described in Thesis 
 * to output popularity score
 */
public class PopularityPrediction {
	
	public static Indexer i = new Indexer();
	
	/**
	 * similarity of project string p to news article in the index
	 * computing similarity based on news article "text" field
	 * currently "text", "title" possible
	 */
	private static double callNews(String p) {
		i.setIndex("news");
		return i.stringSimilarity(new String[] {"text"}, p); 
	}	
	
	/**
	 * methods returns TwitterScore for Project string p
	 * retrieves all Tweets relevant to Project
	 * compute Twitterscore using the retrieved Tweets
	*/
	private static double[] callTwitter(String p) {
		TwitterReader tr = TwitterReader.getInstance(); 
		List<TwitterFeatures> tw = tr.getAllTweets(p); 
		return TwitterReader.computeScore(tw);
	}
	
	/**
	 * aggregation of population scores 
	 * model is described in Thesis
	 * formular for PopularityScore:  log(NewsScore ∗ TwitterScore)
	 * returns array of popularity score, twitter score, news score
	 * avgtext, avgSent, totRet, totfav
	*/
	public static double[] computePopularity(String twitter, String news) {
		//normalization
		double popularityScore = 0; 
		double[] r = new double[7]; 
		
		double i[] = {}; 
		//if no connection to twitter set up set all twitter stats to zero 
		File f = new File("twitterauth.txt");
		if (f.exists() && !f.isDirectory()) {i[0]=0;i[1]=0;i[2]=0;i[3]=0;i[4]=0; }
		// else call TwitterReader
		else { i = callTwitter(twitter); }
		double t = i[0]; 
		double n = callNews(news); 
		if (t>0 && n>0) {popularityScore = Math.log10(t + n); }
		else {if (t>0){t = Math.log10(t); }
		if (n>0){t = Math.log10(n); }
		popularityScore = t + n; 
		}
		//popularityScore = popularityScore + callFacebook(project); 
		r[0] = popularityScore; 
		r[1] = t;
		r[2] = n;
		r[3] = i[1]; 
		r[4] = i[2];
		r[5] = i[3];
		r[6] = i[4];
		return r; 
	}
}


