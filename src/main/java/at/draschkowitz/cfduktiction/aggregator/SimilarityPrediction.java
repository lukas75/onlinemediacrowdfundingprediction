package at.draschkowitz.cfduktiction.aggregator;

import at.draschkowitz.cfduktiction.indexer.Indexer;

/*
 * similarity score is computed by getting project from query or project index
 * and finding similar projects in project index 
 */
public class SimilarityPrediction {
	
	public static double computeSimilarity(String projectQuery) {
		System.out.println("similarity"); 
		double similarityScore = 0; 
		Indexer in = new Indexer(); 
		in.setIndex("project");
		//find similar projects and compute similarity factor
		similarityScore = in.stringSimilarity(new String[] {"title"}, projectQuery); //"description"
		//compute popularity score
		return similarityScore; 
	}
	
}
