package at.draschkowitz.cfduktiction.libtests;
//package at.draschkowitz.cfduktiction.crawler;
//
//import org.apache.http.HttpHost;
//import org.apache.http.auth.AuthScope;
//import org.apache.http.auth.Credentials;
//import org.apache.http.auth.NTCredentials;
//import org.apache.http.conn.params.ConnRoutePNames;
//import org.apache.http.impl.client.DefaultHttpClient;
//import org.freaknet.gtrends.api.GoogleAuthenticator;
//import org.freaknet.gtrends.api.GoogleTrendsClient;
//import org.freaknet.gtrends.api.GoogleTrendsCsvParser;
//import org.freaknet.gtrends.api.GoogleTrendsRequest;
//import org.freaknet.gtrends.api.exceptions.GoogleTrendsClientException;
//
///*
// * I think I have found a way to solve the problem. Just make sure that you call the Google Trends 
// * API with the cookie PREF. That is you don't need to login the Google account. Of course, you don't
// *  need to emulate browser. The cookie PREF is just enough.
//
//OK. Where the cookie PREF comes from? It is very easy. Just open the browser, and login in
// your Google account. Finally, look up the cookie PREF under the Google website, it is just 
// under the domain www.google.com.Then copy the value of the cookie PREF to your program or script. That's all.
//
//I have called the Google Trends API hundreds of times in several seconds by this way. Good Luck to you!
// */
//
//public class TrendApiApp {
//
//    public static void main(String[] args) throws GoogleTrendsClientException {
//        String u = "myuser@gmail.com";
//        String p = "mypasswd";
//        
//        /* OPTIONAL: setup a proxy with NTLM authentication */
//        HttpHost proxy = new HttpHost("proxy.mydomain.com", 8080, "http");
//        Credentials credentials = new NTCredentials("myLogin", "myPasswd", "", "DOMAIN");
//        DefaultHttpClient httpClient = new DefaultHttpClient();
//        httpClient.getCredentialsProvider().setCredentials(new AuthScope(proxy.getHostName(), proxy.getPort()), credentials);
//        httpClient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
//
//        /* Creates a new authenticator */
//        GoogleAuthenticator authenticator = new GoogleAuthenticator(u, p, httpClient);
//        
//        /* Creates a new Google Trends Client */
//        GoogleTrendsClient client = new GoogleTrendsClient(authenticator, httpClient);
//        GoogleTrendsRequest request = new GoogleTrendsRequest("bananas");
//        
//        /* Here the default request params can be modified with getter/setter methods */
//        String content = client.execute(request);
//         
//        /* The default request downloads a CSV available in content */
//        GoogleTrendsCsvParser csvParser = new GoogleTrendsCsvParser(content);
//        /* Get a specific section of the CSV */
//        String section = csvParser.getSectionAsString("Top searches for", true);
//        System.out.println(section);
//    }
//}
