package at.draschkowitz.cfduktiction.libtests;

import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;

import com.rometools.rome.feed.atom.Feed;

import at.draschkowitz.cfduktiction.model.FacebookIndexerObject;
import facebook4j.Account;
import facebook4j.BatchRequest;
import facebook4j.BatchRequests;
import facebook4j.BatchResponse;
import facebook4j.Checkin;
import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.FacebookResponse.Metadata.Fields;
import facebook4j.FacebookResponse.Metadata.Fields.Field;
import facebook4j.GeoLocation;
import facebook4j.Group;
import facebook4j.Like;
import facebook4j.Location;
import facebook4j.Paging;
import facebook4j.Place;
import facebook4j.Post;
import facebook4j.PostUpdate;
import facebook4j.RawAPIResponse;
import facebook4j.Reading;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;
import facebook4j.internal.http.RequestMethod;
import facebook4j.internal.org.json.JSONObject;
import facebook4j.json.DataObjectFactory;

public class FacebookReader {
	
	//dann immer nur ältere posts auslesen und im index speichern so wie bei dings
	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	static Date last_call = java.sql.Date.valueOf(LocalDate.now().minusMonths(1)); //null; 
	static LinkExtractor linkExtractor = LinkExtractor.builder()
	        .linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW))
	        .build();
	
	public static void main (String args[] ) {
		
		try {
	
	facebook4j.Facebook facebook = new FacebookFactory().getInstance();

	facebook.setOAuthAppId("470489256629796", "b2b6fe8d4058e28204fca6dbd2e3f2e3");
	//facebook.setOAuthPermissions("commaSeparetedPermissions");
	//https://stackoverflow.com/questions/16917769/facebook4j-with-oauth-access-token
	//DyRiDOYXTdyy3cKwYMEG7BlzboY
	facebook.setOAuthAccessToken(new AccessToken("470489256629796|b2b6fe8d4058e28204fca6dbd2e3f2e3"));
	 //if (facebook.getOAuthAccessToken() == null) {
	//facebook.setOAuthAccessToken(new AccessToken("470489256629796|DyRiDOYXTdyy3cKwYMEG7BlzboY", null));
	System.out.println(facebook.getOAuthAccessToken()); 
	
//	
//	Facebook facebook = new FacebookFactory().getInstance();
//	// Use default values for oauth app id.
//	facebook.setOAuthAppId("", "");
//	// Get an access token from: 
//	// https://developers.facebook.com/tools/explorer
//	// Copy and paste it below.
//	String accessTokenString = "PASTE_YOUR_ACCESS_TOKEN_STRING_HERE";
//	AccessToken at = new AccessToken(accessTokenString);
//	// Set access token.
//	facebook.setOAuthAccessToken(at);

//	ResponseList<Account> accounts = facebook.getAccounts();
//	Account yourPageAccount = accounts.get(0);  // if index 0 is your page account.
//	String pageAccessToken = yourPageAccount.getAccessToken();

	//not needed for app-access-token
	
//	String shortLivedToken = "your-short-lived-token";
//	AccessToken extendedToken = facebook.extendTokenExpiration(shortLivedToken);
//	Publishing a message
//	You can publish a message via Facebook.postStatusMessage() method.

//	facebook.postStatusMessage("Hello World from Facebook4J.");
////	Publishing a link
////	You can publish a link via Facebook.postFeed() method.
//
//	PostUpdate post = new PostUpdate(new URL("http://facebook4j.org"))
//	                    .picture(new URL("http://facebook4j.org/images/hero.png"))
//	                    .name("Facebook4J - A Java library for the Facebook Graph API")
//	                    .caption("facebook4j.org")
//	                    .description("Facebook4J is a Java library for the Facebook Graph API.");
//	facebook.postFeed(post);
//	Facebook.postLink();  //method is simple way to post.
//
//	facebook.postLink(new URL("http://facebook4j.org"));
//	facebook.postLink(new URL("http://facebook4j.org"), "A Java library for the Facebook Graph API");
////	Getting News Feed
//	Facebook.getHome();  //returns a List of user’s latest News Feed.
//
//	ResponseList<Post> feed = facebook.getHome();
////	Like
////	You can like a Post, Photo, … via Facebook.like****() methods.
//
//	facebook.likePost(postId);
////	Also, You can unlike a Post, Photo, … via Facebook.unlike****() methods.
//
//	facebook.unlikePost(postId);
////	Publising a comment
////	You can comment a Post, Photo, … via Facebook.comment****() methods.
//
//	facebook.commentPhoto(photoId, "It's a nice photo!");
//	Searching
//	You can search for Posts, Users, … via Facebook.search****() methods.

//	Search for public Posts is depricated and no longer possible :(
//		ResponseList<Post> postResults = facebook.ge("crowdfunding watermelon");
//		for (Post p : postResults) {
//			System.out.println(p.getComments().size());
//			System.out.println(p.getDescription());
//			System.out.println(p.getLikes().size());
//			System.out.println(p.getStory());
//			System.out.println(p.getCreatedTime());
//			System.out.println(p.getSharesCount());
//			System.out.println(p.getReactions().size());
//			System.out.println(p.getPlace());
//			System.out.println("---------------------------------");
//		}

//	Search for Users
//	ResponseList<User> results = facebook.searchUsers("mark");
//	Search for Events
//	ResponseList<Event> results = facebook.searchEvents("conference");
	
//	1424030717818617  -  https://www.facebook.com/groups/kickstarterforum/
//	73182029884     -   https://www.facebook.com/Kickstarter
//	7312612866	     -  https://www.facebook.com/Indiegogo
//	319612008170469 -   https://www.facebook.com/pages/Kickstarterforum/319612008170469
//	379417968842871  - 	https://www.facebook.com/pages/Crowdfund-Genius/379417968842871
//	526825367412323 -   https://www.facebook.com/crowdfunding4you
//	372342599496911 -   https://www.facebook.com/groups/372342599496911/	
//	1425143814370643 -  https://www.facebook.com/groups/kickstartergames/
//	455710194508168 -	https://www.facebook.com/groups/455710194508168/
//	138469072961355 -	https://www.facebook.com/groups/KickstarterBestPractices/ missing permissions ? 
//	150217978496903 -	https://www.facebook.com/pages/Crowdfundingpr/150217978496903
//	255559361210581 -   https://www.facebook.com/groups/255559361210581/
//	229135807223236 - 	https://www.facebook.com/groups/229135807223236/
//	394119197296781 -	https://www.facebook.com/groups/ideastoreality/   does not exist any longer
//	431367210304021 -	https://www.facebook.com/groups/crowdfundingpitch/
//	278308035604132 - 	https://www.facebook.com/groups/278308035604132/	
//	233357890166285 - 	https://www.facebook.com/groups/crowdfundingequity/
//	224899674273135	-   https://www.facebook.com/groups/224899674273135/
//	433256503386361 - 	https://www.facebook.com/crowdfundinsider
//	504972496207586 - 	https://www.facebook.com/crowdfundingusa	
//	406734379364170 - 	https://www.facebook.com/launchandrelease
//	800492718  -	    https://www.facebook.com/richardbliss
//	136822083135795	-   https://www.facebook.com/crowdcrux
//	https://findmyfbid.com
//	http://www.crowdcrux.com/23-places-facebook-to-promote-your-kickstarter-or-indiegogo/
		// could also crawl news sites here... 
		
	String[] ids = new String[]
	{"1424030717818617", "73182029884", "7312612866", "319612008170469", "379417968842871", "526825367412323"
	,"372342599496911", "1425143814370643", "455710194508168"//, "138469072961355" 
	,"150217978496903", "255559361210581", "229135807223236", "431367210304021"
	,"278308035604132", "233357890166285", "224899674273135", "433256503386361", "504972496207586"
	,"406734379364170","800492718", "136822083135795"};
	//22 most important crowdfunding pages on facebook according to ^
	
	
	List<FacebookIndexerObject> objectList = new ArrayList<FacebookIndexerObject>(); 
	
	for (String id : ids) {
	// zur sicherheit api limits nicht bekannt - error 4
	try {Thread.sleep(10000);} catch (InterruptedException e) {e.printStackTrace();}
	
	//-----------------------------------------------------------------
	
	// Executing "me" and "me/friends?limit=50" endpoints
	BatchRequests<BatchRequest> batch = new BatchRequests<BatchRequest>();
	batch.add(new BatchRequest(RequestMethod.GET, id));
	//batch.add(new BatchRequest(RequestMethod.GET, "me/friends?limit=50"));
	List<BatchResponse> results = facebook.executeBatch(batch);
	BatchResponse result1 = results.get(0);
	//BatchResponse result2 = results.get(1);

	// You can get http status code or headers
	//int statusCode1 = result1.getStatusCode();
	String usage = result1.getResponseHeader("X-App-Usage");
	System.out.println("X-App-Usage: "+usage); 
	String usage1 = result1.getResponseHeader("X-Page-Usage");
	System.out.println("X-Page-Usage: "+usage1); 
	// You can get body content via as****() method
	String jsonString = result1.asString();
//	JSONObject jsonObject = result1.asJSONObject();
//	ResponseList<JSONObject> responseList = result2.asResponseList();
	// You can map json to java object using DataObjectFactory#create****()
	Group g = DataObjectFactory.createGroup(jsonString);
	//Friend friend1 = DataObjectFactory.createFriend(responseList.get(0).toString());
	//-----------------------------------------------------------------
	
	//time out in case rate limit is 
	//if (usage1 != null || usage != null) Thread.sleep(60000);
	
	//Group g = facebook.getGroup(id);
	System.out.println(g.getDescription()+g.getName()+g.toString()); 
	System.out.println(facebook.getFeed(id).size()); 
	// Getting Next page
	Paging<Post> paging = null; 
	if (last_call == null) paging = facebook.getFeed(id).getPaging();
	//else paging = facebook.getFeed(id, new Reading().since(new Date(new SimpleDateFormat("dd/MM/yyyy").parse("17/07/1989").getTime()))).getPaging();
	else paging = facebook.getFeed(id, new Reading().since(last_call)).getPaging();
	ResponseList<Post> list;
	for  (list = facebook.fetchNext(paging); list!=null; list = facebook.fetchNext(paging)) {
		System.out.println("posts"+list.size());
		for (Post p : list) {
			
			objectList.add(new FacebookIndexerObject(p)); 
			//extract links from the post 
//			Iterable<LinkSpan> links = linkExtractor.extractLinks(p.getMessage());
//			LinkSpan link = links.iterator().next();
//			link.getType();        // LinkType.URL
//			p.getMessage().substring(link.getBeginIndex(), link.getEndIndex());  // "http://test.com	
//			System.out.println("text"+p.getMessage());
//			System.out.println("likes"+p.getLikes().size()); 
//			System.out.println("comments"+p.getComments().size()); 
//			System.out.println("created"+p.getCreatedTime());
//			System.out.println("name"+p.getName());			
//			System.out.println("desc"+p.getDescription());
		}
	}
	
	}
	last_call = new Date(System.currentTimeMillis());  //set date of this call for next fetch
	System.out.println(last_call.toString()); 
	
	
	
//	Search for Groups
//	ResponseList<Group> results = facebook.searchGroups("crowdfunding watermelon");
//	for (Group g : results) {
//		System.out.println(g.getDescription());
//		System.out.println(g.getName());
//		System.out.println(g.getVenue());
//		Iterator a = g.getMetadata().getFields().getFields().iterator(); 
//		while (a.hasNext()) {
//			Field f = (Field) a.next(); 
//			System.out.println(f.getName()+f.getDescription()+"llllllllllllllllllllll"); 
//		}
//		System.out.println("group---------------------------------");
//	}
	
//	facebook.getGroup(groupId);
	
	
	
//	Search for Places
	// Search by name
//	ResponseList<Place> results = facebook.searchPlaces("coffee");

//	// You can narrow your search to a specific location and distance
//	GeoLocation center = new GeoLocation(37.76, -122.427);
//	int distance = 1000;
//	ResponseList<Place> facebook.searchPlaces("coffee", center, distance);
////	Search for Checkins
//	// you or your friend's latest checkins, or checkins where you or your friends have been tagged
//	ResponseList<Checkin> results = facebook.searchCheckins();
////	Search for Locations
//	// To search for objects near a geographical location
//	GeoLocation center = new GeoLocation(37.76, -122.427);
//	int distance = 1000;
//	ResponseList<Location> searchLocations(center, distance);
//
//	// To search for objects at a particular place
//	String placeId = "166793820034304";
//	ResponseList<Location> locations = facebookBestFriend1.searchLocations(placeId);
	} catch (FacebookException e) {
		//https://developers.facebook.com/docs/graph-api/advanced/rate-limiting
		if (e.getErrorCode() == 4) {} //error code = 4, CodedException
		try {
			Thread.sleep(60000);
		} catch (InterruptedException e1) {
			System.out.println("rate limit reached ! "); 
			e1.printStackTrace();
		}
		e.printStackTrace();
	}
	
}
}
