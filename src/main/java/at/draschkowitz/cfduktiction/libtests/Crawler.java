package at.draschkowitz.cfduktiction.libtests;
////taken from tutorial at https://www.mkyong.com/java/jsoup-basic-web-crawler-example/
//
//package at.draschkowitz.cfduktiction.crawler;
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//
//import java.io.IOException;
//import java.io.InputStream;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.HashSet;
//
//public class Crawler {
//
//    //private HashSet<String> links;
//
//    public Crawler() {
//        //links = new HashSet<String>();
//    }
//    
//    public static boolean robotSafe(URL url) 
//    {
//        String strHost = url.getHost();
//
//        String strRobot = "http://" + strHost + "/robots.txt";
//        URL urlRobot;
//        try { urlRobot = new URL(strRobot);
//        } catch (MalformedURLException e) {
//            // something weird is happening, so don't trust it
//            return false;
//        }
//
//        String strCommands;
//        try 
//        {
//            InputStream urlRobotStream = urlRobot.openStream();
//            byte b[] = new byte[1000];
//            int numRead = urlRobotStream.read(b);
//            strCommands = new String(b, 0, numRead);
//            while (numRead != -1) {
//                numRead = urlRobotStream.read(b);
//                if (numRead != -1) 
//                {
//                        String newCommands = new String(b, 0, numRead);
//                        strCommands += newCommands;
//                }
//            }
//           urlRobotStream.close();
//        } 
//        catch (IOException e) 
//        {
//            return true; // if there is no robots.txt file, it is OK to search
//        }
//
//        if (strCommands.contains(DISALLOW)) // if there are no "disallow" values, then they are not blocking anything.
//        {
//            String[] split = strCommands.split("\n");
//            ArrayList<RobotRule> robotRules = new ArrayList<>();
//            String mostRecentUserAgent = null;
//            for (int i = 0; i < split.length; i++) 
//            {
//                String line = split[i].trim();
//                if (line.toLowerCase().startsWith("user-agent")) 
//                {
//                    int start = line.indexOf(":") + 1;
//                    int end   = line.length();
//                    mostRecentUserAgent = line.substring(start, end).trim();
//                }
//                else if (line.startsWith(DISALLOW)) {
//                    if (mostRecentUserAgent != null) {
//                        RobotRule r = new RobotRule();
//                        r.userAgent = mostRecentUserAgent;
//                        int start = line.indexOf(":") + 1;
//                        int end   = line.length();
//                        r.rule = line.substring(start, end).trim();
//                        robotRules.add(r);
//                    }
//                }
//            }
//
//            for (RobotRule robotRule : robotRules)
//            {
//                String path = url.getPath();
//                if (robotRule.rule.length() == 0) return true; // allows everything if BLANK
//                if (robotRule.rule == "/") return false;       // allows nothing if /
//
//                if (robotRule.rule.length() <= path.length())
//                { 
//                    String pathCompare = path.substring(0, robotRule.rule.length());
//                    if (pathCompare.equals(robotRule.rule)) return false;
//                }
//            }
//        }
//        return true;
//    }
//
////    public void getPageLinks(String URL) {
////        //4. Check if you have already crawled the URLs
////        //(we are intentionally not checking for duplicate content in this example)
////        if (!links.contains(URL)) {
////            try {
////                //4. (i) If not add it to the index
////                if (links.add(URL)) {
////                    System.out.println(URL);
////                }
////
////                //2. Fetch the HTML code
////                Document document = Jsoup.connect(URL).get();
////                //3. Parse the HTML to extract links to other URLs
////                Elements linksOnPage = document.select("a[href]");
////
////                //5. For each extracted URL... go back to Step 4.
////                for (Element page : linksOnPage) {
////                    getPageLinks(page.attr("abs:href"));
////                }
////            } catch (IOException e) {
////                System.err.println("For '" + URL + "': " + e.getMessage());
////            }
////        }
////    }
////
////    public static void main(String[] args) {
////        //1. Pick a URL from the frontier
////        //new Crawler().getPageLinks("http://www.mkyong.com/");
////        new Crawler().getPageLinks("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1");
////        //new Crawler().getPageLinks("https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=popular_all&filter_status=&per_page=100&pg_num=100");
////    //indiegogo is schon json 
////    }
//
//}