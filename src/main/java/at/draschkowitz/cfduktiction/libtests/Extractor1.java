//taken from tutorial at https://www.mkyong.com/java/jsoup-basic-web-crawler-example/

package at.draschkowitz.cfduktiction.libtests;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.draschkowitz.cfduktiction.model.IndiegogoProject;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import crawlercommons.robots.BaseRobotRules;
import crawlercommons.robots.SimpleRobotRulesParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Extractor1 {
    private List<IndiegogoProject> projectList; 
    private BaseRobotRules rules; 


	public Extractor1() {
        projectList = new ArrayList<>();
        //int max_crawlDelay = 1000; 

        //create rules from robots.txt file
        URL url = null;
		try {
			url = new URL("https://www.indiegogo.com/robots.txt");
		} catch (MalformedURLException e1) {e1.printStackTrace();}
        InputStream is = null; byte[] bytes = null;
        try {is = url.openStream ();
         bytes = IOUtils.toByteArray(is);
        } catch (IOException e) {} finally {if (is != null) try {is.close();} catch (IOException e) {e.printStackTrace();}}

        SimpleRobotRulesParser robotParser = new SimpleRobotRulesParser();
        rules = robotParser.parseContent("  sdsd ", bytes, "text/plain", "Any-darn-crawler");
    	System.out.println("1"+!rules.isAllowed("")); 
    }


    
    // da jetzt von der project page den api link holen anstatt zu parsen !  
    //Connect to each link saved in the article and find all the articles in the page
    public void retrieveProjects() {
    	StringBuffer result = null;
    	String url = "https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=popular_all&filter_status=&per_page=1&pg_num=1"; 
    	HttpClient client = HttpClientBuilder.create().build();
    	HttpGet request = new HttpGet(url);

    	// add request header
    	request.addHeader("User-Agent", "Mozilla/5.0");
    	//request.addHeader("Accept-Ranges", "bytes=100-1500");
    	//request.addHeader("Accept Encoding", "gzip, deflate");
    	//request.addHeader("Accept Language", "de-at" );
    	//request.addHeader("Request Method", "GET" );
    	
    	HttpResponse response = null;
		try {
			response = client.execute(request);
		

    	System.out.println("Response Code : "
    	                + response.getStatusLine().getStatusCode() + response.getAllHeaders());

    	BufferedReader rd = new BufferedReader(
    		new InputStreamReader(response.getEntity().getContent()));

    	result = new StringBuffer();
    	String line = "";
    	while ((line = rd.readLine()) != null) {
    		result.append(line);
    	}
		} catch (IOException e1) {}
		System.out.println("json : "+result.toString());
		
		String charset = "UTF-8";  // Or in Java 7 and later, use the constant: java.nio.charset.StandardCharsets.UTF_8.name()


		URLConnection connection = null;
		try {
		connection = new URL(url).openConnection();
		connection.setRequestProperty("Accept-Charset", charset);
		InputStream response1 = connection.getInputStream();

		//InputStream response = new URL(url).openStream();


		Scanner scanner = new Scanner(response1) ;
		    String responseBody = scanner.useDelimiter("\\A").next();
		    System.out.println(responseBody);
		
		
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
//    	HttpClient client = new ();
//        GetMethod method = new GetMethod(URL);
//        String html = null; InputStream ios = null;
//        try {
//            int statusCode = client.executeMethod(method);
//            if(statusCode == HttpStatus.SC_REQUESTED_RANGE_NOT_SATISFIABLE) {
//                method.setRequestHeader("User-Agent", "Mozilla/5.0");
//                method.setRequestHeader("Accept-Ranges", "bytes=100-1500");
//                statusCode = client.executeMethod(method);
//            }
//            ios = method.getResponseBodyAsStream();
//            html = IOUtils.toString(ios, "utf-8");
//            System.out.println(statusCode);
//        }catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if(ios!=null) {
//                try {ios.close();} 
//                catch (IOException e) {e.printStackTrace();}
//            }
//            if(method!=null) method.releaseConnection();
//        }
//
//        System.out.println(html);
    	//http response code 416 
			        ObjectMapper mapper = new ObjectMapper();
			        try {
			        	//maximum 100 pro page (0-99)
					//IndiegogoProject project = mapper.readValue(result.toString(), IndiegogoProject.class);
					IndiegogoProject project = mapper.readValue(new URL("https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=popular_all&filter_status=&per_page=1&pg_num=1"), IndiegogoProject.class);
					projectList.add(project); 
//			        //1 Convert a JSON Array format to a Java List object. for indiegogo
//			        List<IndiegogoProject> list = mapper.readValue(new URL("https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=popular_all&filter_status=&per_page=10&pg_num=1"), new TypeReference<List<IndiegogoProject>>(){});
//			        projectList.addAll(list); 
			        //error handlen eigentlich
			        } catch (Exception e) {System.err.println(e.getMessage());}
			        
//			    } catch (IOException e) {System.err.println(e.getMessage());}
//			}
			//});
    }
    
    public List<IndiegogoProject> updateProjects() {
		return projectList; 
    }


    public static void main(String[] args) {
        Extractor1 bwc = new Extractor1();
        bwc.retrieveProjects();
        System.out.println(bwc.projectList.get(0).toString()); 
        System.out.println(bwc.projectList.size()); 
        
    }
}


