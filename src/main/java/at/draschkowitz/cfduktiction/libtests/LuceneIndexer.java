//http://computergodzilla.blogspot.co.at/2015/01/calculate-cosine-similarity-using.html

package at.draschkowitz.cfduktiction.libtests;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.search.similarities.Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.LockObtainFailedException;

import at.draschkowitz.cfduktiction.model.KickstarterProject;


/**
 * Class to create Lucene Index from files.
 * Remember this class will only index files inside a folder.
 * If there are  multiple folder inside the source folder it will not index 
 * those files.
 * 
 *  It will only index text files 
 * @author Mubin Shrestha
 */
public class LuceneIndexer {
	
  public static final String INDEX_DIRECTORY = "lucene/projects";
  public static final String FIELD_CONTENT = "contents"; // name of the field to index

    private final File indexDirectory;
    private static String fieldName;
    
//  private Directory dir; 
//  private  Analyzer analyzer;  

    public LuceneIndexer() {
        this.indexDirectory = new File(INDEX_DIRECTORY);
        //fieldName = FIELD_CONTENT;
        fieldName = "description"; 
//        try {dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));} catch (IOException e) {e.printStackTrace();}
//        analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words 
    }
    
    public void addToIndex(String a) {
    	try {
    	Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

        if (indexDirectory.exists()) {
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        } else {
            // Add new documents to an existing index:
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        }

        IndexWriter writer = new IndexWriter(dir, iwc);
            Document doc = new Document();
            FieldType fieldType = new FieldType();
            fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            fieldType.setStored(true);
            fieldType.setStoreTermVectors(true);
            fieldType.setTokenized(true);
            Field contentField = new Field(fieldName, a, fieldType);
            doc.add(contentField);
            //doc.add(new StringField("id", id, Field.Store.YES));
            writer.addDocument(doc);
        
            writer.commit(); 
            writer.close();
    	}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
    }
    
    public void similarProjects(String field) {
    	//fieldName = field; 
    	try {
            Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));       
        			// Searching code
        		    IndexReader reader = DirectoryReader.open(dir);
        		    MoreLikeThis mlt = new MoreLikeThis(reader);
        		    mlt.setMinDocFreq(0);
        		    mlt.setMinTermFreq(0);
        		    IndexSearcher searcher = new IndexSearcher(reader);
        		    System.out.println("Found files: "+ reader.getDocCount(field));
        		    		for (int i=0; i<reader.maxDoc(); i++) {
        		    		    //if (reader.	isDeleted(i)) continue;
        		    		    Document doc = reader.document(i);
        		    		    String s = doc.get(field);
        		    		    System.out.println(s); 
        	        		    Query likeQuery = mlt.like(i);
        	        		    System.out.println("query: "+likeQuery.toString(field)); 
        	        		    TopDocs results = searcher.search(likeQuery, 5);
        		    		    // do something with docId here...
        	        		    ScoreDoc[] hits = results.scoreDocs; 
        	        		    //	Code to display the results of search
        	        		    System.out.println("Found " + hits.length + " hits.");
        	        		    for (int j=0;j<hits.length;j++) {
        	        		      int docId = hits[j].doc;
        	        		      Document d = searcher.doc(docId);
        	        		      System.out.println((j + 1) + ". " + d.get("title")+" "+hits[j].score+d.get(field) + "\t" );
        	        	
        	        		    }
        		    		}
        		    
        		    //Reader target = null;  // orig source of doc you want to find similarities to
        		    //Query query = mlt.like( target);

        		    


        		    

        		    
        		    // reader can only be closed when there is no need to access the documents any more
        		    reader.close();
        		}
        		catch(Exception e)
        		{
        			System.out.println(e.getMessage());
        		}

    }
    
    public void search() {//throws CorruptIndexException, LockObtainFailedException, IOException {
    	try {
    	
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words

//    	Text to search   
    			String querystr = "Everybody loves fruits, and some of us love even more turtles !Combine both of them to enjoy tiny pineturtle and waturtlemelon pins.";
    			//String querystr = "turtles"; 
    			
    			//	The \"title\" arg specifies the default field to use when no field is explicitly specified in the query
    			Query q = new QueryParser(FIELD_CONTENT, analyzer).parse(querystr);
    			
    			// Searching code
    			int hitsPerPage = 10;
    		    IndexReader reader = DirectoryReader.open(dir);
    		    IndexSearcher searcher = new IndexSearcher(reader);
    		    //Similarity s = searcher.getSimilarity(true); 
    		    
    		    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
    		    searcher.search(q, collector);
    		    ScoreDoc[] hits = collector.topDocs().scoreDocs;
    		    
    		    //	Code to display the results of search
    		    System.out.println("Found " + hits.length + " hits.");
    		    for (int i=0;i<hits.length;++i) {
    		      int docId = hits[i].doc;
    		      Document d = searcher.doc(docId);
    		      System.out.println((i + 1) + ". " + d.get(FIELD_CONTENT) + "\t" + d.get("title")+" "+hits[i].score);
    		    }
    		    
    		    // reader can only be closed when there is no need to access the documents any more
    		    reader.close();
    		}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    		}
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /**
     * Method to create Lucene Index
     * Keep in mind that always index text value to Lucene for calculating 
     * Cosine Similarity.
     * You have to generate tokens, terms and their frequencies and store
     * them in the Lucene Index.
     */
    public void index(List<KickstarterProject> text) {//throws CorruptIndexException, LockObtainFailedException, IOException {
    	try {
    	
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);

        if (indexDirectory.exists()) {
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
        } else {
            // Add new documents to an existing index:
            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        }

        IndexWriter writer = new IndexWriter(dir, iwc);
        for (KickstarterProject f : text) {
            Document doc = new Document();
            FieldType fieldType = new FieldType();
            fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
            fieldType.setStored(true);
            fieldType.setStoreTermVectors(true);
            fieldType.setTokenized(true);
            Field contentField = new Field("contents", f.getBlurb(), fieldType);
            Field contentField1 = new Field("description", f.getDescription(), fieldType);
            Field contentField2 = new Field("title", f.getName(), fieldType);
            doc.add(contentField);doc.add(contentField1);doc.add(contentField2); 
            
            
            //doc.add(new StringField("id", id, Field.Store.YES));
            doc.add(new StringField("country", f.getCountry(), Field.Store.YES));
            doc.add(new StringField("challenges", f.getChallenges(), Field.Store.YES));
            doc.add(new StringField("currency", f.getCurrency(), Field.Store.YES));
            doc.add(new StringField("slug", f.getSlug(), Field.Store.YES));
            doc.add(new StringField("state", f.getState(), Field.Store.YES));
            doc.add(new StringField("backersCount", f.getBackersCount().toString(), Field.Store.YES));
            doc.add(new StringField("plegdedusd", f.getUsdPledged(), Field.Store.YES));
            doc.add(new StringField("comments", f.getCommentsCount().toString(), Field.Store.YES));
            doc.add(new StringField("createdat", f.getCreatedAt().toString(), Field.Store.YES));
            doc.add(new StringField("updatesCount", f.getUpdatesCount().toString(), Field.Store.YES));
            doc.add(new StringField("deadline", f.getDeadline().toString(), Field.Store.YES));
            doc.add(new StringField("goal", f.getGoal().toString(), Field.Store.YES));
            doc.add(new StringField("country", f.getLaunchedAt().toString(), Field.Store.YES));
            doc.add(new StringField("pledged", f.getPledged().toString(), Field.Store.YES));
            doc.add(new StringField("staticRate", f.getStaticUsdRate().toString(), Field.Store.YES));
            doc.add(new StringField("staffpick", f.getStaffPick().toString(), Field.Store.YES));
            
            writer.addDocument(doc);
        }
        writer.commit(); 
        writer.close();
        
//    	Text to search
        
    			String querystr = "Everybody loves fruits, and some of us love even more turtles !Combine both of them to enjoy tiny pineturtle and waturtlemelon pins.";
    			
    			//	The \"title\" arg specifies the default field to use when no field is explicitly specified in the query
    			Query q = new QueryParser("title", analyzer).parse(querystr);
    			
    			// Searching code
    			int hitsPerPage = 10;
    		    IndexReader reader = DirectoryReader.open(dir);
    		    IndexSearcher searcher = new IndexSearcher(reader);
    		    //Similarity s = searcher.getSimilarity(true); 
    		    //s.simScorer(s.computeWeight(searcher.termStatistics("", reader.getContext()), searcher.collectionStatistics("")) , searcher.getTopReaderContext()); 
    		    TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage);
    		    searcher.search(q, collector);
    		    ScoreDoc[] hits = collector.topDocs().scoreDocs;
    		    
    		    
    		    //	Code to display the results of search
    		    System.out.println("Found " + hits.length + " hits.");
    		    for (int i=0;i<hits.length;++i) {
    		      int docId = hits[i].doc;
    		      Document d = searcher.doc(docId);
    		      System.out.println((i + 1) + ". " + d.get("isbn") + "\t" + d.get("title")+" "+hits[i].score);
    		    }
    		    
    		    // reader can only be closed when there is no need to access the documents any more
    		    reader.close();
    		}
    		catch(Exception e)
    		{
    			System.out.println(e.getMessage());
    		}
        
    }

}

//wenns rennt mach i a configuration class - aber am besten gleich für alle libraries
//Below is the Configuration Class: 
//// Configuration.java
//package com.computergodzilla.cosinesimilairty;
//
///**
// * @author Mubin Shrestha
// */
//public class Configuration { 
//    public static final String SOURCE_DIRECTORY_TO_INDEX = "E:/TEST";
//    public static final String INDEX_DIRECTORY = "E:/INDEXDIRECTORY";
//    public static final String FIELD_CONTENT = "contents"; // name of the field to index
//}


//
////http://computergodzilla.blogspot.co.at/2015/01/calculate-cosine-similarity-using.html
//
//package at.draschkowitz.cfduktiction.model;
//
//import java.io.*;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//
//import org.apache.lucene.analysis.Analyzer;
//import org.apache.lucene.analysis.standard.StandardAnalyzer;
//import org.apache.lucene.document.Document;
//import org.apache.lucene.document.Field;
//import org.apache.lucene.document.FieldType;
//import org.apache.lucene.index.CorruptIndexException;
//import org.apache.lucene.index.IndexOptions;
//import org.apache.lucene.index.IndexWriter;
//import org.apache.lucene.index.IndexWriterConfig;
//import org.apache.lucene.store.Directory;
//import org.apache.lucene.store.FSDirectory;
//import org.apache.lucene.store.LockObtainFailedException;
//
//
///**
//* Class to create Lucene Index from files.
//* Remember this class will only index files inside a folder.
//* If there are  multiple folder inside the source folder it will not index 
//* those files.
//* 
//*  It will only index text files 
//* @author Mubin Shrestha
//*/
//public class LuceneIndexer {
//	
//public static final String SOURCE_DIRECTORY_TO_INDEX = "/Users/lukasdraschkowitz/Downloads/TEST";
//public static final String INDEX_DIRECTORY = "/Users/lukasdraschkowitz/Downloads/TEST/INDEXDIRECTORY";
//public static final String FIELD_CONTENT = "contents"; // name of the field to index
//
//  private final File sourceDirectory;
//  private final File indexDirectory;
//  private static String fieldName;
//
//  public LuceneIndexer() {
////      this.sourceDirectory = new File(Configuration.SOURCE_DIRECTORY_TO_INDEX);
////      this.indexDirectory = new File(Configuration.INDEX_DIRECTORY);
////      fieldName = Configuration.FIELD_CONTENT;
//      this.sourceDirectory = new File(SOURCE_DIRECTORY_TO_INDEX);
//      this.indexDirectory = new File(INDEX_DIRECTORY);
//      fieldName = FIELD_CONTENT;
//  }
//
//  /**
//   * Method to create Lucene Index
//   * Keep in mind that always index text value to Lucene for calculating 
//   * Cosine Similarity.
//   * You have to generate tokens, terms and their frequencies and store
//   * them in the Lucene Index.
//   * @throws CorruptIndexException
//   * @throws LockObtainFailedException
//   * @throws IOException 
//   */
//  public void index() throws CorruptIndexException,
//          LockObtainFailedException, IOException {
//      Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
//      Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
//      IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
//
//      if (indexDirectory.exists()) {
//          iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
//      } else {
//          // Add new documents to an existing index:
//          iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
//      }
//
//      IndexWriter writer = new IndexWriter(dir, iwc);
//      for (File f : sourceDirectory.listFiles()) {
//          Document doc = new Document();
//          FieldType fieldType = new FieldType();
//          //fieldType.setIndexed(true);
//          fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
//          fieldType.setStored(true);
//          fieldType.setStoreTermVectors(true);
//          fieldType.setTokenized(true);
//          Field contentField = new Field(fieldName, getAllText(f), fieldType);
//          doc.add(contentField);
//          writer.addDocument(doc);
//      }
//      writer.close();
//  }
//
//  /**
//   * Method to get all the texts of the text file.
//   * Lucene cannot create the term vetors and tokens for reader class.
//   * You have to index its text values to the index.
//   * It would be more better if this was in another class.
//   * I am lazy you know all.
//   * @param f
//   * @return
//   * @throws FileNotFoundException
//   * @throws IOException 
//   */
//  public String getAllText(File f) throws FileNotFoundException, IOException {
//      String textFileContent = "";
//
//      for (String line : Files.readAllLines(Paths.get(f.getAbsolutePath()))) {
//          textFileContent += line;
//      }
//      return textFileContent;
//  }
//}
//
////wenns rennt mach i a configuration class - aber am besten gleich für alle libraries
////Below is the Configuration Class: 
//////Configuration.java
////package com.computergodzilla.cosinesimilairty;
////
/////**
////* @author Mubin Shrestha
////*/
////public class Configuration { 
////  public static final String SOURCE_DIRECTORY_TO_INDEX = "E:/TEST";
////  public static final String INDEX_DIRECTORY = "E:/INDEXDIRECTORY";
////  public static final String FIELD_CONTENT = "contents"; // name of the field to index
////}