package at.draschkowitz.cfduktiction.libtests;

/*
 * Database Credentials
Get credentials for manual connections to this database.
Please note that these credentials are not permanent.
Heroku rotates credentials periodically and updates applications where this database is attached.
Host
ec2-54-75-235-2.eu-west-1.compute.amazonaws.com
Database
d3he10la6seubn
User
tjoulpypufijeh
Port
5432
Password
04fd09026a0a701454fbbb384bb44dee58006a06df67cc3d78a225dd4275aafd
URI
postgres://tjoulpypufijeh:04fd09026a0a701454fbbb384bb44dee58006a06df67cc3d78a225dd4275aafd@ec2-54-75-235-2.eu-west-1.compute.amazonaws.com:5432/d3he10la6seubnHeroku
heroku pg:psql postgresql-globular-85840 --app crowdfundingservice
*/

import java.sql.*;
import java.util.*;

public class Database {

	  class Blog
	  {
	    public int id;
	    public String subject;
	    public String permalink;
	  }

	  public static void main(String[] args)
	  {
	    new Database();
	  }

	  public Database() 
	  {
	    Connection conn = null;
	    LinkedList listOfBlogs = new LinkedList();

	    // connect to the database
	    conn = connectToDatabaseOrDie();

	    // get the data
	    populateListOfTopics(conn, listOfBlogs);
	    
	    // print the results
	    printTopics(listOfBlogs);
	  }
	  
	  private void printTopics(LinkedList listOfBlogs)
	  {
	    Iterator it = listOfBlogs.iterator();
	    while (it.hasNext())
	    {
	      Blog blog = (Blog)it.next();
	      System.out.println("id: " + blog.id + ", subject: " + blog.subject);
	    }
	  }

	  private void populateListOfTopics(Connection conn, LinkedList listOfBlogs)
	  {
	    try 
	    {
	      Statement st = conn.createStatement();
	      ResultSet rs = st.executeQuery("SELECT id, subject, permalink FROM blogs ORDER BY id");
	      while ( rs.next() )
	      {
	        Blog blog = new Blog();
	        blog.id        = rs.getInt    ("id");
	        blog.subject   = rs.getString ("subject");
	        blog.permalink = rs.getString ("permalink");
	        listOfBlogs.add(blog);
	      }
	      rs.close();
	      st.close();
	    }
	    catch (SQLException se) {
	      System.err.println("Threw a SQLException creating the list of blogs.");
	      System.err.println(se.getMessage());
	    }
	  }

	  private Connection connectToDatabaseOrDie()
	  {
	    Connection conn = null;
	    try
	    {
	      Class.forName("org.postgresql.Driver");
	      String url = "jdbc:postgresql://ec2-54-75-235-2.eu-west-1.compute.amazonaws.com:5432/d3he10la6seubn";
	      //conn = DriverManager.getConnection(url,"tjoulpypufijeh", "04fd09026a0a701454fbbb384bb44dee58006a06df67cc3d78a225dd4275aafd");
	      
	      Properties props = new Properties();
	      props.setProperty("user","tjoulpypufijeh");
	      props.setProperty("password","04fd09026a0a701454fbbb384bb44dee58006a06df67cc3d78a225dd4275aafd");
	      props.setProperty("sslnode","require");
	      props.setProperty("ssl","true");
	      props.setProperty("sslfactory", "org.postgresql.ssl.NonValidatingFactory");
	      conn = DriverManager.getConnection(url, props);
	      
	    }
	    catch (ClassNotFoundException e)
	    {
	      e.printStackTrace();
	      System.exit(1);
	    }
	    catch (SQLException e)
	    {
	      e.printStackTrace();
	      System.exit(2);
	    }
	    return conn;
	  }

}
