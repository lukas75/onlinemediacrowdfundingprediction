package at.draschkowitz.cfduktiction.libtests;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.status.StatusListener;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.neural.rnn.RNNCoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations.SentimentAnnotatedTree;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.util.CoreMap;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.RateLimitStatus;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterSent {
	
	public static ArrayList<String> getTweets(String topic) {

	    ConfigurationBuilder cb = new ConfigurationBuilder();
	    cb.setDebugEnabled(true);
	    cb.setOAuthConsumerKey("y6xgVLeyrNpu2CtOBagVWu5P9");
	    cb.setOAuthConsumerSecret("zRhhgh9MNpNC4gk5EDmw9KXd9msiz5FnNL3ubkkTnoTdRa1ycR");
	    cb.setOAuthAccessToken("274610097-znzE5dYDD9cLUassiIKrbmIOIxE5gauSSPuobKmE");
	    cb.setOAuthAccessTokenSecret("EsGBvfiLbiS0kXIZ1HOranRmgzonM3BdcyWbwcRKaG9RH");
	    
		Twitter twitter = new TwitterFactory(cb.build()).getInstance();
		ArrayList<String> tweetList = new ArrayList<String>();
		
		Map<String, RateLimitStatus> r; 
		RateLimitStatus rls; 
		try {
			Query query = new Query(topic);
			QueryResult result;

			do {
				 r = twitter.getRateLimitStatus("search");
					rls = r.get(("/search/tweets")); 
					System.out.println("while: limit"+rls.getLimit()+"remaining: "+rls.getRemaining()); 
				result = twitter.search(query);
				List<Status> tweets = result.getTweets();
				for (Status tweet : tweets) {
					 r = twitter.getRateLimitStatus("search");
						rls = r.get(("/search/tweets")); 
					System.out.println("for: limit"+rls.getLimit()+"remaining: "+rls.getRemaining()); 
					tweetList.add(tweet.getText());
				}
			} while ((query = result.nextQuery()) != null);
			
			 r = twitter.getRateLimitStatus("search");
				rls = r.get(("/search/tweets")); 
				System.out.println("DONE: limit"+rls.getLimit()+"remaining: "+rls.getRemaining()); 
				
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to search tweets: " + te.getMessage());
		}

		return tweetList;
	    
//	        TwitterStream twitterStream = new TwitterStreamFactory(cb.build()).getInstance();
//	        StatusListener listener = new StatusListener() {
//
//	            public void onStatus(Status status) {
//	                System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
//	            }
//
//	            public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
//	                System.out.println("Got a status deletion notice id:" + statusDeletionNotice.getStatusId());
//	            }
//
//	            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
//	                System.out.println("Got track limitation notice:" + numberOfLimitedStatuses);
//	            }
//
//	            public void onScrubGeo(long userId, long upToStatusId) {
//	                System.out.println("Got scrub_geo event userId:" + userId + " upToStatusId:" + upToStatusId);
//	            }
//
//	            public void onException(Exception ex) {
//	                ex.printStackTrace();
//	            }
//	        };
//
//	        FilterQuery fq = new FilterQuery();
//	        String keywords[] = {"France", "Germany"};
//
//	        fq.track(keywords);
//
//	        twitterStream.addListener(listener);
//	        twitterStream.filter(fq);      
	        
	}
	

	static StanfordCoreNLP pipeline;

	public static void init() {
		Properties props = new Properties();
		props.setProperty("annotators", "tokenize, ssplit, parse, sentiment");
		//The default Stanford PCFG parser is cubic time complexity with respect to the sentence length. 
		//This is why we usually recommend restricting the maximum sentence length for performance reasons
		props.setProperty("parse.maxlen", "20"); 
//		props.setProperty("tokenize.options", "untokenizable=noneDelete");
		pipeline = new StanfordCoreNLP(props);
	}

	public static int findSentiment(String tweet) {

		int mainSentiment = 0;
		if (tweet != null && tweet.length() > 0) {
			int longest = 0;
			Annotation annotation = pipeline.process(tweet);
			for (CoreMap sentence : annotation
					.get(CoreAnnotations.SentencesAnnotation.class)) {
				Tree tree = sentence
						.get(SentimentAnnotatedTree.class);
				int sentiment = RNNCoreAnnotations.getPredictedClass(tree);
				String partText = sentence.toString();
				if (partText.length() > longest) {
					mainSentiment = sentiment;
					longest = partText.length();
				}

			}
		}
		return mainSentiment;
	}	
	
	public static void main(String[] args) {
		String topic = "crowdfunding new"; //6 to 9 days old 
		ArrayList<String> tweets = getTweets(topic);
		init();
		for(String tweet : tweets) {
			System.out.println(tweet + " : " + findSentiment(tweet));
		}
	}
	
}
