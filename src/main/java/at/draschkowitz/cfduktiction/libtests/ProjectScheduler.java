package at.draschkowitz.cfduktiction.libtests;

import java.util.Calendar;
import java.util.List;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.digitalpebble.stormcrawler.ConfigurableTopology;

//import at.draschkowitz.cfduktiction.crawler.FbReader;
import at.draschkowitz.cfduktiction.crawler.KickstarterReader;
import at.draschkowitz.cfduktiction.crawler.StormCrawler;
import at.draschkowitz.cfduktiction.indexer.Indexer;
import at.draschkowitz.cfduktiction.model.FacebookIndexerObject;

public class ProjectScheduler {
	
    public static void startUp() throws Exception {
        SchedulerFactory sf = new StdSchedulerFactory();
        Scheduler scheduler = sf.getScheduler();
        scheduler.start();
        
        JobDetail newsJob = JobBuilder.newJob(NewsJob.class).withIdentity("news", "group1").build();
        JobDetail cfJob = JobBuilder.newJob(CFJob.class).withIdentity("cf", "group1").build();
        JobDetail fbJob = JobBuilder.newJob(FBJob.class).withIdentity("fb", "group1").build();
        
        Trigger trigger_1 = TriggerBuilder.newTrigger()
                .withIdentity("cf", "group1")
//                .withSchedule(CronScheduleBuilder.cronSchedule("0,30 * * ? * MON-FRI")) /*Run every 30 seconds on Weekdays (Monday through Friday)*/
                .withSchedule(CronScheduleBuilder.weeklyOnDayAndHourAndMinute(1, 0, 0))
                .build();
        Trigger trigger_2 = TriggerBuilder.newTrigger()
                .withIdentity("fb", "group1")
                .withSchedule(CronScheduleBuilder.monthlyOnDayAndHourAndMinute(1, 1, 0))
                .build();
        Trigger trigger_3 = TriggerBuilder.newTrigger()
                .withIdentity("news", "group1")
                .withSchedule(CronScheduleBuilder.dailyAtHourAndMinute(1, 0))
                .build();
        
        scheduler.addJob(JobBuilder.newJob(NewsJob.class).withIdentity("news", "group0").storeDurably().build(), true);
        scheduler.addJob(JobBuilder.newJob(CFJob.class).withIdentity("cf", "group0").storeDurably().build(), true);
        scheduler.addJob(JobBuilder.newJob(FBJob.class).withIdentity("fb", "group0").storeDurably().build(), true);
        scheduler.triggerJob(JobKey.jobKey("news", "group0"));
        //scheduler.triggerJob(JobKey.jobKey("cf", "group0"));
        //scheduler.triggerJob(JobKey.jobKey("fb", "group0"));
        
        scheduler.scheduleJob(newsJob, trigger_3);
        scheduler.scheduleJob(cfJob, trigger_1);
        scheduler.scheduleJob(fbJob, trigger_2);
//check update functions cf, and only crawl new news sites and fb posts !!!!!! 
        
    }
	
//	//create or add to indizes
//	public void startCrawl() {
//		
//		//crawl news sites asynchronisously in threads
//		ConfigurableTopology.start(new StormCrawler(), new String[]{"-conf crawlerconf.yaml","-local"} );
//		
//    	new Thread(new Runnable() {
//			@Override
//			public void run() {
//				//crawl Facebook 
//				Indexer in = new Indexer(); 
//				in.setIndex("facebook");
//				FbReader fb = new FbReader(); 
//				List<FacebookIndexerObject> a = fb.getPosts(); 
//				in.index(a);
//			}
//		}).start();
//
//		
//    	new Thread(new Runnable() {
//			@Override
//			public void run() {
//				// crawl Kickstarter 
//		    	KickstarterReader bwc = new KickstarterReader();
//		        bwc.getPageLinks("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1"); 
//		        bwc.retrieveProjects();
//				Indexer in = new Indexer(); 
//				in.setIndex("kickstarter");
//				in.index(bwc.getProjectList());
//			}
//		}).start();
//
//
//		
//		//crawl google trends api 
//		//not yet implemented 
//	}
	
	//read from indizes and from twitter 
	//returns double similarity and double popularity score
	public double[] readFromIndex(String query) {
		return null; 
	}
	
	//or update Machine Learning component
	public void sendToMLComponent() {
		
	}
	
//	@Async
//	public Future<String> findPage(String page) throws InterruptedException {
//		String results = "done"; 
//	    Thread.sleep(1000L);
//	    return new AsyncResult<String>(results);
//	}
	
	//only add to index in case its not already there ! 
    public static void main(String[] args) {
    	
//    	KickstarterReader bwc = new KickstarterReader();
//        bwc.getPageLinks(""); 
//        bwc.retrieveProjects();
//		Indexer in1 = new Indexer(); 
//		in1.setIndex("project");
//		in1.index(bwc.getProjectList());
//
//		Indexer in = new Indexer(); 
//		in.setIndex("facebook");
//		FbReader fb = new FbReader(); 
//		List<FacebookIndexerObject> a = fb.getPosts(); 
//		in.index(a);
//        
//        //start news crawler
//        ConfigurableTopology.start(new StormCrawler(), new String[]{"-conf crawlerconf.yaml","-local"} );
        
        
//        Indexer in = new Indexer(); 
//        in.setIndex("news"); 
//        in.similarProjects(new String [] {"title"});
        
        //PopularityPrediction.computePopularity("king queen knife"); 
        //SimilarityPrediction.computeSimilarity("king queen knife"); 
        
    	
    	try {
    		startUp(); 
    		while (true) {
			Thread.sleep(999999999);}
		} catch (Exception e) {e.printStackTrace();} 
        

    }


public static class CFJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
    	System.out.println("cf started"); 
//    	new Thread(new Runnable() {
//			@Override
//			public void run() {
				// crawl Kickstarter 
		    	KickstarterReader bwc = new KickstarterReader();
//		        bwc.getPageLinks("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1"); 
//		        bwc.retrieveProjects();
				Indexer in = new Indexer(); 
				in.setIndex("project");
				
//				bwc.readProjectsFromFile(
//						new File("/Users/lukasdraschkowitz/git/cf/Spring4MVCAngularJSExample/target/classes/com/websystique/springmvc/controller/logic/crawl/kickstarterfiles.json")
//						, KickstarterProject.class); 
				
//				bwc.readProjectsFromFile(
//						new File("")
//						, IndiegogoProject.class); 
				
//				in.index(bwc.getProjectList());
//				System.out.println("cf indexed"+Calendar.getInstance().getTime().toString()); 
//			}
//		}).start();
    }
}

public static class FBJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
//    	new Thread(new Runnable() {
//			@Override
//			public void run() {
				//crawl Facebook 
    	System.out.println("fb started"); 
				Indexer in = new Indexer(); 
				in.setIndex("facebook");
//				FbReader fb = new FbReader(); 
//				List<FacebookIndexerObject> a = fb.getPosts();  
//				in.index(a);
//				System.out.println("fb indexed"+Calendar.getInstance().getTime().toString()); 
//			}
//		}).start();
    }

}

public static class NewsJob implements Job {

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
		//crawl news sites asynchronisously in threads
    	System.out.println("news started"); 
		ConfigurableTopology.start(new StormCrawler(), new String[]{"-conf crawlerconf.yaml","-local"} );
		System.out.println("news indexed"+Calendar.getInstance().getTime().toString()); 
    }

}


}
//System.out.println(bwc.getProjectList().get(0).toString()); 
//System.out.println(bwc.getProjectList().size()); 
//LuceneIndexer li = new LuceneIndexer(); 
//li.index(bwc.getProjectList());
//
//li.addToIndex("turtles"); 
//li.addToIndex("Everybody loves fruits, and some of us love even more turtles !Combine both of them to enjoy tiny pineturtle and waturtlemelon pins.");
//li.search(); 

//li.similarProjects("title"); 

//----------------------------------------------