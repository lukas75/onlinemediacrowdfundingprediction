package at.draschkowitz.cfduktiction.libtests;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.html.HtmlParser;
import org.apache.tika.sax.BodyContentHandler;

import org.xml.sax.SAXException;

public class Fetch {
	
//	try {
//        InputStream input = new FileInputStream(new File(path));
//        ContentHandler textHandler = new BodyContentHandler();
//        Metadata metadata = new Metadata();
//        AutoDetectParser parser = new AutoDetectParser();
//        ParseContext context = new ParseContext();
//        parser.parse(input, textHandler, metadata, context);
//        System.out.println("Title: " + metadata.get(metadata.TITLE));
//        System.out.println("Body: " + textHandler.toString());
//    } catch (FileNotFoundException e) {
//        e.printStackTrace();
//    } catch (IOException e) {
//        e.printStackTrace();
//    } catch (SAXException e) {
//        e.printStackTrace();
//    } catch (TikaException e) {
//        e.printStackTrace();
//    }

   public static void main(final String[] args) throws IOException,SAXException, TikaException {

      //detecting the file type
      BodyContentHandler handler = new BodyContentHandler();
      Metadata metadata = new Metadata();
      //FileInputStream inputstream = new FileInputStream(new File("/Users/lukasdraschkowitz/Downloads/A film.html"));
      //InputStream inputstream = new URL("https://www.indiegogo.com/private_api/explore?filter_funding=&filter_percent_funded=&filter_quick=popular_all&filter_status=&per_page=1&pg_num=1").openStream();
      InputStream inputstream = new URL("http://abcnews.go.com/International/wireStory/presidents-scandal-brazilians-pondering-government-47525093").openStream();

      ParseContext pcontext = new ParseContext();
      
      //Html parser 
      HtmlParser htmlparser = new HtmlParser();
      htmlparser.parse(inputstream, handler, metadata,pcontext);
      System.out.println("Contents of the document:" + handler.toString());
      System.out.println("Metadata of the document:");
      String[] metadataNames = metadata.names();
      
      for(String name : metadataNames) {
         System.out.println(name + ":   " + metadata.get(name));  
      }
   }
}