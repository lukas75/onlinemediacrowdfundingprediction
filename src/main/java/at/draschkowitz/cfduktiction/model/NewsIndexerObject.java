package at.draschkowitz.cfduktiction.model;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;

import com.digitalpebble.stormcrawler.Metadata;

public class NewsIndexerObject implements IndexingObject {
	
	private String text; 
	private String title; 
	//private String author; 
	//private String description;
	//private String[] keywords; 
	private String url; 
	//private String facebook_title; 
	//private Date date; //of crawl
	
	private String date; 
	//private String fb_app_id; 
	
	public NewsIndexerObject(Metadata metadata) {
		this.text = ""; 
		this.title =""; 
		this.url = "";
        for (String fieldName : metadata.keySet()){ 
            String[] values = metadata.getValues(fieldName);
            if (values != null) {
            for (String value : values) {
                System.out.println("META: "+fieldName + "\t" + trimValue(value));
            	if (fieldName.equals("title")) {this.title = value != null ? value : "";}
            	if (fieldName.equals("content")) {this.text = value != null ? value : "";}
            	if (fieldName.equals("url")) {this.url = value != null ? value : "";}
            	if (fieldName.equals("date") && value !=null) {this.date = value != null ? value : "";}
            }
            }
        }
	}

@Override
public Document getDocument() {
	Document doc = new Document();
    FieldType fieldType = new FieldType();
    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
    fieldType.setStored(true);
    fieldType.setStoreTermVectors(true);
    fieldType.setTokenized(true);
    Field contentField = new Field("text", this.text, fieldType);
    Field contentField3 = new Field("url", this.url, fieldType);
    //dow mon dd hh:mm:ss zzz yyyy
    Field contentField2 = new Field("date", this.date.toString(), fieldType);
    doc.add(contentField);doc.add(contentField2);doc.add(contentField3); //doc.add(contentField1);
    doc.add(new StringField("title", title, Field.Store.YES));
	return doc;
}

private String trimValue(String a) {
	return a; 
}

}

/**
 * possible metadata
copyright:   Copyright (c) 2017 ABC News Internet Ventures
og:site_name:   ABC News
keywords:   entertainment news, celebrity interviews, hollywood stars, music concerts, movie reviews, reality TV, television reviews, pop culture, entertainment news, celebrity news, celebrity gossip
author:   ABC News
og:title:   Entertainment Index
description:   Get up to the minute  entertainment news, celebrity interviews, celeb videos, photos,  movies, TV,  music news and pop culture on ABCNews.com.
title:   Entertainment News |Latest Celebrity News, Videos & Photos - ABC News - ABC News
og:description:   Get up to the minute  entertainment news, celebrity interviews, celeb videos, photos,  movies, TV,  music news and pop culture on ABCNews.com.
X-UA-Compatible:   IE=edge,chrome=1
fb:app_id:   4942312939
viewport:   initial-scale=1.0, maximum-scale=1.0, user-scalable=no
dc:title:   Entertainment News |Latest Celebrity News, Videos & Photos - ABC News - ABC News
Content-Encoding:   UTF-8
fb:admins:   704409894
robots:   noarchive
og:url:   http://abcnews.go.com/Entertainment
fb_title:   Entertainment Index
Content-Type:   text/html; charset=UTF-8
50188 [Thread-34-parse-executor[4 4]] INFO  c.w.s.l.MyJsoupParserBolt - Parsed http://abcnews.go.com/Entertainment in 33 msec text: Sections Sections Top Stories Watch U.S. International Politics Lifestyle Entertainment Virtual Reality Health Tech Investigative Sports Weather Shows Shows Good Morning America World News Tonight Nightline 20/20 This Week What Would You Do? Live Live Empire State Building lit yellow in honor of Alliance for Young Artists and Writers RADAR: Severe storms in the Southeast Sydney Opera House in Sydney Harbor Alpacas graze at the Stargazer Ranch in Loveland, CO Kittens nap and play on 'Dorm Cam' in Los Angeles Friends of Forsythe osprey cam in Oceanville, NJ Live look at the surf in Rockaway Beach, NY Atlantic coast from Gloucester, Mass from the Blue Shutters Beachside Inn More Privacy Policy Your CA Privacy Rights Children's Online Privacy Policy Interest-Based Ads Terms of Use Contact Us Yahoo!-ABC News Network | © 2017 ABC News Internet Ventures. All rights reserved. Search Menu ABC News Log In U.S. International Politics Lifestyle Entertainment Virtual Reality … … Entertainment Virtual Reality Health Tech Investigative Sports Weather Privacy PolicyPrivacy Policy Your CA Privacy RightsYour CA Privacy Rights Children's Online Privacy PolicyChildren's Online Privacy Policy Interest-Based AdsInterest-Based Ads Terms of UseTerms of Use Contact UsContact Us Yahoo!-ABC News Network | © 2017 ABC News Internet Ventures. All rights reserved. Shows Good Morning America Good Morning America World News Tonight World News Tonight Nightline Nightline 20/20 20/20 This Week This Week What Would You Do? What Would You Do? Live Watch Entertainment News The Associated Press Oliver Stone: Megyn Kelly didn't know her stuff with Putin Jun 7 Top Entertainment Stories ABC is off and running with NBA Finals Israel's in love with its homegrown Wonder Woman Gal Gadot George Clooney's dad talks about the twins Cosby accuser testifies for 7 hours in sexual assault case Val Kilmer is eager to play 'Iceman' again in new 'Top Gun' Review: In 'The Mummy,' Tom Cruise dances with the undead Jerry Seinfeld says 'no thanks' to hug from Kesha New trial requested in 'Making a Murderer' case Ariana Grande resumes her tour in Paris David Bowie's widow pays tribute to her late husband Latest Entertainment Video 4:23 Warriors fans gear up for Game 3 of the NBA Finals 26:06 Behind the scenes of Tony-nominated 'The Great Comet' 2:58 Esa-Pekka Salonen coaches musicians from Ensemble Connect at Carnegie Hall 1:44 Cosby accuser testifies for 7 hours in sexual assault case 8:10 Kevin Hart weighs in on Kathy Griffin, Bill Maher controversies 18:49 The songwriters behind 'Dear Evan Hansen,' 'La La Land' on how they tell stories 1:51 'Dear Evan Hansen' creators Benj Pasek and Justin Paul sing 'Waving Through a Window' 2:51 'Working Girl' and more new musicals coming to Broadway
EXECUTION
TEXT: content	2777 chars
URL: url	http://abcnews.go.com/Entertainment
META: date	Thu, 08 Jun 2017 08:46:11 GMT
META: server	Apache-Coyote/1.1
META: copyright	Copyright (c) 2017 ABC News Internet Ventures
META: og:site_name	ABC News
META: keywords	187 chars
META: vary	User-Agent, Accept-Encoding
META: robots.noFollow	false
META: description	142 chars
META: title	Entertainment News |Latest Celebrity News, Videos & Photos - ABC News - ABC News
META: og:description	142 chars
META: via	1.1 varnish
META: http.trimmed	true
META: dc:title	Entertainment News |Latest Celebrity News, Videos & Photos - ABC News - ABC News
META: Content-Encoding	UTF-8
META: fb:admins	704409894
META: content-type	text/html;charset=utf8
META: connection	Keep-Alive
META: from	n7nwswc01.starwave.com
META: robots	noarchive
META: cache-control	max-age=300
META: fb_title	Entertainment Index
META: Content-Type	text/html; charset=UTF-8
META: x-ua-compatible	IE=edge,chrome=1
META: robots.noIndex	false
META: author	ABC News
META: og:title	Entertainment Index
META: parse.Content-Encoding	ISO-8859-1
META: x-varnish	1613469945 1613443508
META: fetch.statusCode	200
META: X-UA-Compatible	IE=edge,chrome=1
META: robots.noCache	true
META: fb:app_id	4942312939
META: url.path	105 chars
META: url.path	http://abcnews.go.com
META: depth	2
META: x-vg-webcache	169
META: fetch.loadingTime	174
META: viewport	initial-scale=1.0, maximum-scale=1.0, user-scalable=no
META: x-cache	HIT
META: parse.Content-Type	text/html; charset=utf8
META: accept-ranges	bytes
META: og:url	http://abcnews.go.com/Entertainment
META: age	293
*/

