package at.draschkowitz.cfduktiction.model;

import java.util.ArrayList;
import java.util.Date;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.index.IndexOptions;

import twitter4j.Status;

public class TwitterFeatures implements IndexingObject {
	
	String text; 
	String language; 
	int favoriteCount; 
	int retweetCount; 
	Date createdAt;
	int sentiment; 
	ArrayList<String> hashtags; 
	ArrayList<String> urls; 
	ArrayList<String> users; 
	
	public TwitterFeatures(Status s, int sent) {
		//all i need from a tweet	
	      favoriteCount = s.getFavoriteCount(); 
		  //hashtags = null; hashtags = s.getHashtagEntities(); 
		  //urls = null; // urls = s.getURLEntities();  
//		  s.getUserMentionEntities()
//		  s.getCreatedAt().toString(),
//		  s.getUser().getScreenName(),
	      createdAt = s.getCreatedAt(); 
		retweetCount = s.getRetweetCount(); 
		language = s.getLang(); 
		text= cleanText(s.getText()); 
		this.sentiment = sent; 
	}
	
	/**
	 * Replace newlines and tabs in text with escaped versions to making printing cleaner
	 *
	 * @param text	The text of a tweet, sometimes with embedded newlines and tabs
	 * @return		The text passed in, but with the newlines and tabs replaced
	 */
	public static String cleanText(String text)
	{
		text = text.replace("\n", "\\n");
		text = text.replace("\t", "\\t");

		return text;
	}
	
	@Override
	public Document getDocument() {
		Document doc = new Document();
	    FieldType fieldType = new FieldType();
	    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
	    fieldType.setStored(true);
	    fieldType.setStoreTermVectors(true);
	    fieldType.setTokenized(true);
	    Field contentField = new Field("text", this.text, fieldType);
	    Field contentField1 = new Field("url", this.createdAt.toString(), fieldType);
	    Field contentField2 = new Field("language", this.language, fieldType);
	    doc.add(contentField);doc.add(contentField1);doc.add(contentField2); 
	    //rest of the fields only for storage
	    doc.add(new IntPoint("retweetcount", this.retweetCount));
	    doc.add(new IntPoint("favoriteCount", this.favoriteCount));
	    doc.add(new IntPoint("sentiment", this.sentiment));
		return doc;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getFavoriteCount() {
		return favoriteCount;
	}

	public void setFavoriteCount(int favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	public int getRetweetCount() {
		return retweetCount;
	}

	public void setRetweetCount(int retweetCount) {
		this.retweetCount = retweetCount;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public int getSentiment() {
		return sentiment;
	}

	public void setSentiment(int sentiment) {
		this.sentiment = sentiment;
	}

	public ArrayList<String> getHashtags() {
		return hashtags;
	}

	public void setHashtags(ArrayList<String> hashtags) {
		this.hashtags = hashtags;
	}

	public ArrayList<String> getUrls() {
		return urls;
	}

	public void setUrls(ArrayList<String> urls) {
		this.urls = urls;
	}

	public ArrayList<String> getUsers() {
		return users;
	}

	public void setUsers(ArrayList<String> users) {
		this.users = users;
	}

}
