package at.draschkowitz.cfduktiction.model;

import java.util.Date;
import java.util.EnumSet;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.IntPoint;
import org.apache.lucene.document.IntRangeField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;
import org.nibor.autolink.LinkExtractor;
import org.nibor.autolink.LinkSpan;
import org.nibor.autolink.LinkType;

import facebook4j.Post;


/*creates features to be indexed in lucene index from Facebook Data */ 
public class FacebookIndexerObject implements IndexingObject{
	
	private Post post;
	private String text; 
	private int likes; 
	private int comments; 
	private Date created; 
	private String name; 
	private String desc; 
	private String url; 
	private String id; 
	
	static LinkExtractor linkExtractor = LinkExtractor.builder()
	        .linkTypes(EnumSet.of(LinkType.URL, LinkType.WWW))
	        .build();
	
	public FacebookIndexerObject(Post p) {
		url = ""; 
		if (p.getMessage()!=null) {
			Iterable<LinkSpan> links = linkExtractor.extractLinks(p.getMessage());
			if (links.iterator().hasNext()) {
				LinkSpan link = links.iterator().next();
				link.getType();        // LinkType.URL
				url = p.getMessage().substring(link.getBeginIndex(), link.getEndIndex());  // "http://test.com	
			}
		}
		text = p.getMessage() != null ? p.getMessage() : "";
		likes = p.getLikes().size(); 
		comments = p.getComments().size(); 
		created = p.getCreatedTime() != null ? p.getCreatedTime() : new Date();
		name = p.getName() != null ? p.getName() : "";			
		desc = p.getDescription() != null ? p.getDescription() : "";
		id = p.getId() != null ? p.getId() : ""; 
	}

@Override
public Document getDocument() {
	Document doc = new Document();
	//selbst definiertes Field 
    FieldType fieldType = new FieldType();
    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
    fieldType.setStored(true);
    fieldType.setStoreTermVectors(true);
    fieldType.setTokenized(true);
    Field contentField = new Field("text", this.text, fieldType);
    Field contentField1 = new Field("url", this.url, fieldType);
    Field contentField2 = new Field("title", this.name, fieldType);
    doc.add(contentField);doc.add(contentField1);doc.add(contentField2); 
    //rest of the fields only for storage
    doc.add(new StringField("id", this.id, Field.Store.YES));
    doc.add(new StringField("created", this.created.toString(), Field.Store.YES));
    doc.add(new StoredField("comments", this.comments)); //IntPoint
    doc.add(new StoredField("likes", this.likes));
	return doc;
}

}
