package at.draschkowitz.cfduktiction.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
//import com.fasterxml.jackson.annotation.JsonTypeInfo;

/*
 * Superclass for all Classes realizing a Crowdfunding Platform Object
 * (These are currently KickstarterProject and IndigogoProject)
 *  */


//@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.WRAPPER_OBJECT, property="@class")
@JsonPropertyOrder({
	"pid",
	"conceptname",
	"platform",
	"score",
	"simScore",
	"popScore",
	"twitterStat",
//	"fbStat",
	"newsStat"
})
public class Project implements Comparable<Project>{

		@JsonIgnore
		private static int classid = 0; 
		@JsonProperty("pid")
		private Integer pid;
		@JsonProperty("conceptname")
		private String conceptname;
		@JsonProperty("platform")
		private String platform;
		@JsonProperty("score")
		private int score; 
		@JsonProperty("simScore")
		private double simScore; 
		@JsonProperty("popScore")
		private double popScore; 
		@JsonProperty("twitterStat")
		private int twitterStat; 
//		@JsonProperty("fbStat")
//		private int fbStat; 
		@JsonProperty("newsStat")
		private int newsStat; 
		@JsonProperty("avgText")//avg tweet text length
		private String avgText="";
		@JsonProperty("avgSent")
		private String avgSent="";
		@JsonProperty("totRetweet")
		private String totRetweet="";
		@JsonProperty("totFav")
		private String totFav="";
		
		public Project(){
			this.conceptname = "";
			this.platform = "";
			this.pid = classid; classid++; 
			this.score = -1; 
		}
		
		public Project(String conceptname, String platform){
			this.pid = classid; classid++; 
			this.conceptname = conceptname;
			this.platform = platform;
			this.score = -1; 
		}
		
		public Project(String conceptname, String platform, int score){
			this.pid = classid; classid++; 
			this.conceptname = conceptname;
			this.platform = platform;
			this.score = score;  
		}
		
		@JsonProperty("pid")
		public Integer getPid() {
			return pid;
		}
		@JsonProperty("pid")
		public void setPid(Integer Pid) {
			this.pid = Pid;
		}

		public String getConceptname() {
			return conceptname;
		}

		public void setConceptname(String conceptname) {
			this.conceptname = conceptname;
		}

		public String getPlatform() {
			return platform;
		}

		public void setPlatform(String platform) {
			this.platform = platform;
		}
		
		public int getScore() {
			return score;
		}

		public void setScore(int score) {
			this.score = score;
		}


		public double getSimScore() {
			return simScore;
		}

		public void setSimScore(double simScore) {
			this.simScore = simScore;
		}

		public double getPopScore() {
			return popScore;
		}

		public void setPopScore(double popScore) {
			this.popScore = popScore;
		}

		public int getTwitterStat() {
			return twitterStat;
		}

		public void setTwitterStat(int twitterStat) {
			this.twitterStat = twitterStat;
		}

//		public int getFbStat() {
//			return fbStat;
//		}
//
//		public void setFbStat(int fbStat) {
//			this.fbStat = fbStat;
//		}

		public int getNewsStat() {
			return newsStat;
		}

		public void setNewsStat(int newsStat) {
			this.newsStat = newsStat;
		}
		@JsonIgnore
		public String getAvgText() {
			return avgText;
		}
		@JsonIgnore
		public void setAvgText(String avgText) {
			this.avgText = avgText;
		}
		@JsonIgnore
		public String getAvgSent() {
			return avgSent;
		}
		@JsonIgnore
		public void setAvgSent(String avgSent) {
			this.avgSent = avgSent;
		}
		@JsonIgnore
		public String getTotRetweet() {
			return totRetweet;
		}
		@JsonIgnore
		public void setTotRetweet(String totRetweet) {
			this.totRetweet = totRetweet;
		}
		@JsonIgnore
		public String getTotFav() {
			return totFav;
		}
		@JsonIgnore
		public void setTotFav(String totFav) {
			this.totFav = totFav;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + (int) (pid ^ (pid >>> 32));
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (!(obj instanceof Project))
				return false;
			Project other = (Project) obj;
			if (pid != other.pid)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "CFProject [id=" + pid + ", conceptname=" + conceptname + ", platform=" + platform
					 + ", score=" + score + "]";
		}

		@Override
		public int compareTo(Project o) {
			return Integer.compare(o.score, this.score);
		}
		
	}
