package at.draschkowitz.cfduktiction.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"description",
"minimum",
"reward",
"title_for_backing_tier",
"title",
"starts_at",
"ends_at",
"shipping_enabled",
"estimated_delivery_on",
"project_id",
"backers_count",
"updated_at",
"rewards_items",
"urls",
"limit",
"shipping_preference",
"shipping_summary",
"remaining"
})
public class Reward {

@JsonProperty("id")
private Integer id;
@JsonProperty("description")
private String description;
@JsonProperty("minimum")
private Double minimum;
@JsonProperty("reward")
private String reward;
@JsonProperty("title_for_backing_tier")
private String titleForBackingTier;
@JsonProperty("title")
private String title;
@JsonProperty("starts_at")
private Integer startsAt;
@JsonProperty("ends_at")
private Integer endsAt;
@JsonProperty("shipping_enabled")
private Boolean shippingEnabled;
@JsonProperty("estimated_delivery_on")
private Integer estimatedDeliveryOn;
@JsonProperty("project_id")
private Integer projectId;
@JsonProperty("backers_count")
private Integer backersCount;
@JsonProperty("updated_at")
private Integer updatedAt;
@JsonProperty("rewards_items")
private List<Object> rewardsItems = null;
@JsonProperty("limit")
private Integer limit;
@JsonProperty("shipping_preference")
private String shippingPreference;
@JsonProperty("shipping_summary")
private String shippingSummary;
@JsonProperty("remaining")
private Integer remaining;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

/**
* No args constructor for use in serialization
* 
*/
public Reward() {
}

/**
* 
* @param limit
* @param startsAt
* @param shippingPreference
* @param shippingEnabled
* @param reward
* @param minimum
* @param urls
* @param endsAt
* @param projectId
* @param backersCount
* @param updatedAt
* @param id
* @param title
* @param remaining
* @param rewardsItems
* @param titleForBackingTier
* @param description
* @param estimatedDeliveryOn
* @param shippingSummary
*/
public Reward(Integer id, String description, Double minimum, String reward, String titleForBackingTier, String title, Integer startsAt, 
		Integer endsAt, Boolean shippingEnabled, Integer estimatedDeliveryOn, Integer projectId, Integer backersCount, Integer updatedAt, 
		List<Object> rewardsItems, Integer limit, String shippingPreference, String shippingSummary, Integer remaining) {
super();
this.id = id;
this.description = description;
this.minimum = minimum;
this.reward = reward;
this.titleForBackingTier = titleForBackingTier;
this.title = title;
this.startsAt = startsAt;
this.endsAt = endsAt;
this.shippingEnabled = shippingEnabled;
this.estimatedDeliveryOn = estimatedDeliveryOn;
this.projectId = projectId;
this.backersCount = backersCount;
this.updatedAt = updatedAt;
this.rewardsItems = rewardsItems;
this.limit = limit;
this.shippingPreference = shippingPreference;
this.shippingSummary = shippingSummary;
this.remaining = remaining;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("description")
public String getDescription() {
return description;
}

@JsonProperty("description")
public void setDescription(String description) {
this.description = description;
}

@JsonProperty("minimum")
public Double getMinimum() {
return minimum;
}

@JsonProperty("minimum")
public void setMinimum(Double minimum) {
this.minimum = minimum;
}

@JsonProperty("reward")
public String getReward() {
return reward;
}

@JsonProperty("reward")
public void setReward(String reward) {
this.reward = reward;
}

@JsonProperty("title_for_backing_tier")
public String getTitleForBackingTier() {
return titleForBackingTier;
}

@JsonProperty("title_for_backing_tier")
public void setTitleForBackingTier(String titleForBackingTier) {
this.titleForBackingTier = titleForBackingTier;
}

@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@JsonProperty("starts_at")
public Integer getStartsAt() {
return startsAt;
}

@JsonProperty("starts_at")
public void setStartsAt(Integer startsAt) {
this.startsAt = startsAt;
}

@JsonProperty("ends_at")
public Integer getEndsAt() {
return endsAt;
}

@JsonProperty("ends_at")
public void setEndsAt(Integer endsAt) {
this.endsAt = endsAt;
}

@JsonProperty("shipping_enabled")
public Boolean getShippingEnabled() {
return shippingEnabled;
}

@JsonProperty("shipping_enabled")
public void setShippingEnabled(Boolean shippingEnabled) {
this.shippingEnabled = shippingEnabled;
}

@JsonProperty("estimated_delivery_on")
public Integer getEstimatedDeliveryOn() {
return estimatedDeliveryOn;
}

@JsonProperty("estimated_delivery_on")
public void setEstimatedDeliveryOn(Integer estimatedDeliveryOn) {
this.estimatedDeliveryOn = estimatedDeliveryOn;
}

@JsonProperty("project_id")
public Integer getProjectId() {
return projectId;
}

@JsonProperty("project_id")
public void setProjectId(Integer projectId) {
this.projectId = projectId;
}

@JsonProperty("backers_count")
public Integer getBackersCount() {
return backersCount;
}

@JsonProperty("backers_count")
public void setBackersCount(Integer backersCount) {
this.backersCount = backersCount;
}

@JsonProperty("updated_at")
public Integer getUpdatedAt() {
return updatedAt;
}

@JsonProperty("updated_at")
public void setUpdatedAt(Integer updatedAt) {
this.updatedAt = updatedAt;
}

@JsonProperty("rewards_items")
public List<Object> getRewardsItems() {
return rewardsItems;
}

@JsonProperty("rewards_items")
public void setRewardsItems(List<Object> rewardsItems) {
this.rewardsItems = rewardsItems;
}

@JsonProperty("limit")
public Integer getLimit() {
return limit;
}

@JsonProperty("limit")
public void setLimit(Integer limit) {
this.limit = limit;
}

@JsonProperty("shipping_preference")
public String getShippingPreference() {
return shippingPreference;
}

@JsonProperty("shipping_preference")
public void setShippingPreference(String shippingPreference) {
this.shippingPreference = shippingPreference;
}

@JsonProperty("shipping_summary")
public String getShippingSummary() {
return shippingSummary;
}

@JsonProperty("shipping_summary")
public void setShippingSummary(String shippingSummary) {
this.shippingSummary = shippingSummary;
}

@JsonProperty("remaining")
public Integer getRemaining() {
return remaining;
}

@JsonProperty("remaining")
public void setRemaining(Integer remaining) {
this.remaining = remaining;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@Override
public String toString() {
	return "Reward [id=" + id + ", description=" + description + ", minimum=" + minimum + ", reward=" + reward
			+ ", titleForBackingTier=" + titleForBackingTier + ", title=" + title + ", startsAt=" + startsAt
			+ ", endsAt=" + endsAt + ", shippingEnabled=" + shippingEnabled + ", estimatedDeliveryOn="
			+ estimatedDeliveryOn + ", projectId=" + projectId + ", backersCount=" + backersCount + ", updatedAt="
			+ updatedAt + ", rewardsItems=" + rewardsItems + ", limit=" + limit + ", shippingPreference="
			+ shippingPreference + ", shippingSummary=" + shippingSummary + ", remaining=" + remaining
			+ ", additionalProperties=" + additionalProperties + "]";
}

}