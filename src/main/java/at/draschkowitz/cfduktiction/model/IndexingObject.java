package at.draschkowitz.cfduktiction.model;

import org.apache.lucene.document.Document;

/**
 * subclasses create features from Data to be indexed in Lucene index 
 * */ 
public interface IndexingObject {
	
	public abstract Document getDocument();

}
