//-----------------------------------at.draschkowitz.cfduktiction.model.IndiegogoProject.java-----------------------------------

package at.draschkowitz.cfduktiction.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.SortedNumericDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"title",
"nearest_five_percent",
"tagline",
"cached_collected_pledges_count",
"igg_image_url",
"compressed_image_url",
"balance",
"currency_code",
"amt_time_left",
"url",
"category_url",
"category_name",
"category_slug",
"card_type",
"collected_percentage",
"partner_name",
"in_forever_funding",
"friend_contributors",
"friend_team_members"
})
public class IndiegogoProject extends Project implements IndexingObject{

@JsonProperty("id")
private Integer id;
@Override
public String toString() {
	return "IndiegogoProject [id=" + id + ", title=" + title + ", nearestFivePercent=" + nearestFivePercent
			+ ", tagline=" + tagline + ", cachedCollectedPledgesCount=" + cachedCollectedPledgesCount + ", iggImageUrl="
			+ iggImageUrl + ", compressedImageUrl=" + compressedImageUrl + ", balance=" + balance + ", currencyCode="
			+ currencyCode + ", amtTimeLeft=" + amtTimeLeft + ", url=" + url + ", categoryUrl=" + categoryUrl
			+ ", categoryName=" + categoryName + ", categorySlug=" + categorySlug + ", cardType=" + cardType
			+ ", collectedPercentage=" + collectedPercentage + ", partnerName=" + partnerName + ", inForeverFunding="
			+ inForeverFunding + ", friendContributors=" + friendContributors + ", friendTeamMembers="
			+ friendTeamMembers + ", additionalProperties=" + additionalProperties + "]";
}

@JsonProperty("title")
private String title;
@JsonProperty("nearest_five_percent")
private Integer nearestFivePercent;
@JsonProperty("tagline")
private String tagline;
@JsonProperty("cached_collected_pledges_count")
private Integer cachedCollectedPledgesCount;
@JsonProperty("igg_image_url")
private String iggImageUrl;
@JsonProperty("compressed_image_url")
private String compressedImageUrl;
@JsonProperty("balance")
private String balance;
@JsonProperty("currency_code")
private String currencyCode;
@JsonProperty("amt_time_left")
private String amtTimeLeft;
@JsonProperty("url")
private String url;
@JsonProperty("category_url")
private String categoryUrl;
@JsonProperty("category_name")
private String categoryName;
@JsonProperty("category_slug")
private String categorySlug;
@JsonProperty("card_type")
private String cardType;
@JsonProperty("collected_percentage")
private String collectedPercentage;
@JsonProperty("partner_name")
private Object partnerName;
@JsonProperty("in_forever_funding")
private Boolean inForeverFunding;
@JsonProperty("friend_contributors")
private List<Object> friendContributors = null;
@JsonProperty("friend_team_members")
private List<Object> friendTeamMembers = null;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

//next 4 are actually part of superclass Project
@JsonProperty("avgText")//avg twitter text length
private String avgText="";
@JsonProperty("avgSent")
private String avgSent="";
@JsonProperty("totRetweet")
private String totRetweet="";
@JsonProperty("totFav")
private String totFav="";

/**
* No args constructor for use in serialization
* 
*/
public IndiegogoProject() {
}

/**
* 
* @param partnerName
* @param compressedImageUrl
* @param currencyCode
* @param friendContributors
* @param cardType
* @param categorySlug
* @param nearestFivePercent
* @param collectedPercentage
* @param url
* @param id
* @param inForeverFunding
* @param categoryName
* @param balance
* @param title
* @param iggImageUrl
* @param cachedCollectedPledgesCount
* @param friendTeamMembers
* @param categoryUrl
* @param amtTimeLeft
* @param tagline
*/
public IndiegogoProject(Integer id, String title, Integer nearestFivePercent, String tagline, Integer cachedCollectedPledgesCount, String iggImageUrl, String compressedImageUrl, String balance, String currencyCode, String amtTimeLeft, String url, String categoryUrl, String categoryName, String categorySlug, String cardType, String collectedPercentage, Object partnerName, Boolean inForeverFunding, List<Object> friendContributors, List<Object> friendTeamMembers) {
super();
this.id = id;
this.title = title;
this.nearestFivePercent = nearestFivePercent;
this.tagline = tagline;
this.cachedCollectedPledgesCount = cachedCollectedPledgesCount;
this.iggImageUrl = iggImageUrl;
this.compressedImageUrl = compressedImageUrl;
this.balance = balance;
this.currencyCode = currencyCode;
this.amtTimeLeft = amtTimeLeft;
this.url = url;
this.categoryUrl = categoryUrl;
this.categoryName = categoryName;
this.categorySlug = categorySlug;
this.cardType = cardType;
this.collectedPercentage = collectedPercentage;
this.partnerName = partnerName;
this.inForeverFunding = inForeverFunding;
this.friendContributors = friendContributors;
this.friendTeamMembers = friendTeamMembers;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@JsonProperty("nearest_five_percent")
public Integer getNearestFivePercent() {
return nearestFivePercent;
}

@JsonProperty("nearest_five_percent")
public void setNearestFivePercent(Integer nearestFivePercent) {
this.nearestFivePercent = nearestFivePercent;
}

@JsonProperty("tagline")
public String getTagline() {
return tagline;
}

@JsonProperty("tagline")
public void setTagline(String tagline) {
this.tagline = tagline;
}

@JsonProperty("cached_collected_pledges_count")
public Integer getCachedCollectedPledgesCount() {
return cachedCollectedPledgesCount;
}

@JsonProperty("cached_collected_pledges_count")
public void setCachedCollectedPledgesCount(Integer cachedCollectedPledgesCount) {
this.cachedCollectedPledgesCount = cachedCollectedPledgesCount;
}

@JsonProperty("igg_image_url")
public String getIggImageUrl() {
return iggImageUrl;
}

@JsonProperty("igg_image_url")
public void setIggImageUrl(String iggImageUrl) {
this.iggImageUrl = iggImageUrl;
}

@JsonProperty("compressed_image_url")
public String getCompressedImageUrl() {
return compressedImageUrl;
}

@JsonProperty("compressed_image_url")
public void setCompressedImageUrl(String compressedImageUrl) {
this.compressedImageUrl = compressedImageUrl;
}

@JsonProperty("balance")
public String getBalance() {
return balance;
}

@JsonProperty("balance")
public void setBalance(String balance) {
this.balance = balance;
}

@JsonProperty("currency_code")
public String getCurrencyCode() {
return currencyCode;
}

@JsonProperty("currency_code")
public void setCurrencyCode(String currencyCode) {
this.currencyCode = currencyCode;
}

@JsonProperty("amt_time_left")
public String getAmtTimeLeft() {
return amtTimeLeft;
}

@JsonProperty("amt_time_left")
public void setAmtTimeLeft(String amtTimeLeft) {
this.amtTimeLeft = amtTimeLeft;
}

@JsonProperty("url")
public String getUrl() {
return url;
}

@JsonProperty("url")
public void setUrl(String url) {
this.url = url;
}

@JsonProperty("category_url")
public String getCategoryUrl() {
return categoryUrl;
}

@JsonProperty("category_url")
public void setCategoryUrl(String categoryUrl) {
this.categoryUrl = categoryUrl;
}

@JsonProperty("category_name")
public String getCategoryName() {
return categoryName;
}

@JsonProperty("category_name")
public void setCategoryName(String categoryName) {
this.categoryName = categoryName;
}

@JsonProperty("category_slug")
public String getCategorySlug() {
return categorySlug;
}

@JsonProperty("category_slug")
public void setCategorySlug(String categorySlug) {
this.categorySlug = categorySlug;
}

@JsonProperty("card_type")
public String getCardType() {
return cardType;
}

@JsonProperty("card_type")
public void setCardType(String cardType) {
this.cardType = cardType;
}

@JsonProperty("collected_percentage")
public String getCollectedPercentage() {
return collectedPercentage;
}

@JsonProperty("collected_percentage")
public void setCollectedPercentage(String collectedPercentage) {
this.collectedPercentage = collectedPercentage;
}

@JsonProperty("partner_name")
public Object getPartnerName() {
return partnerName;
}

@JsonProperty("partner_name")
public void setPartnerName(Object partnerName) {
this.partnerName = partnerName;
}

@JsonProperty("in_forever_funding")
public Boolean getInForeverFunding() {
return inForeverFunding;
}

@JsonProperty("in_forever_funding")
public void setInForeverFunding(Boolean inForeverFunding) {
this.inForeverFunding = inForeverFunding;
}

@JsonProperty("friend_contributors")
public List<Object> getFriendContributors() {
return friendContributors;
}

@JsonProperty("friend_contributors")
public void setFriendContributors(List<Object> friendContributors) {
this.friendContributors = friendContributors;
}

@JsonProperty("friend_team_members")
public List<Object> getFriendTeamMembers() {
return friendTeamMembers;
}

@JsonProperty("friend_team_members")
public void setFriendTeamMembers(List<Object> friendTeamMembers) {
this.friendTeamMembers = friendTeamMembers;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

@JsonIgnore
public String getAvgText() {
	return avgText;
}
@JsonIgnore
public void setAvgText(String avgText) {
	this.avgText = avgText;
}
@JsonIgnore
public String getAvgSent() {
	return avgSent;
}
@JsonIgnore
public void setAvgSent(String avgSent) {
	this.avgSent = avgSent;
}
@JsonIgnore
public String getTotRetweet() {
	return totRetweet;
}
@JsonIgnore
public void setTotRetweet(String totRetweet) {
	this.totRetweet = totRetweet;
}
@JsonIgnore
public String getTotFav() {
	return totFav;
}
@JsonIgnore
public void setTotFav(String totFav) {
	this.totFav = totFav;
}

@Override
@JsonIgnore
public Document getDocument() {
//	System.out.println("getdocumentducment function called dont know why -.-"); 
	
	Document doc = new Document();

    FieldType fieldType = new FieldType();
    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
    fieldType.setStored(true);
    fieldType.setStoreTermVectors(true);
    fieldType.setTokenized(true);
    Field contentField2 = new Field("title", this.getTitle(), fieldType);
    doc.add(contentField2); 
    //doc.add(new StringField("id", id, Field.Store.YES));

    doc.add(new SortedNumericDocValuesField("iscore", this.getScore()));
    doc.add(new SortedNumericDocValuesField("isimscore", (int) this.getSimScore()));
    doc.add(new SortedNumericDocValuesField("ipopscore", (int) this.getPopScore()));
    doc.add(new StoredField("score", this.getScore()));
    doc.add(new StoredField("simscore", this.getSimScore()));
    doc.add(new StoredField("popscore", this.getPopScore()));
    
    doc.add(new StringField("newsstat", String.valueOf(this.getNewsStat()), Field.Store.YES));
    doc.add(new StringField("twitterstat", String.valueOf(this.getTwitterStat()), Field.Store.YES));
    
    doc.add(new StringField("avgText", String.valueOf(this.getAvgText()), Field.Store.YES));
    doc.add(new StringField("avgSent", String.valueOf(this.getAvgSent()), Field.Store.YES));
    doc.add(new StringField("totRetweet", String.valueOf(this.getTotRetweet()), Field.Store.YES));
    doc.add(new StringField("totFav", String.valueOf(this.getTotFav()), Field.Store.YES));
    
	return doc; 
}

@JsonIgnore
public IndiegogoProject(Document doc) {
	this.title = doc.get("title");

    if (doc.get("score") != null) {this.setScore(Integer.parseInt(doc.get("score")));} else {this.setScore(0); }
//    super.setScore(Integer.parseInt(doc.get("score")));
    if (doc.get("simscore") != null) { this.setSimScore(Double.parseDouble(doc.get("simscore")));} else {this.setSimScore(0);}
    if (doc.get("popscore") != null) { this.setPopScore(Double.parseDouble(doc.get("popscore")));} else {this.setPopScore(0);}
    super.setConceptname(this.title);
    super.setPlatform("Indiegogo");
    
    if (doc.get("twitterstat") != null) {this.setTwitterStat(Integer.parseInt(doc.get("twitterstat"))); }
    if (doc.get("newsstat") != null) {this.setNewsStat(Integer.parseInt(doc.get("newsstat"))); }
    
    if (doc.get("avgText") != null) {this.setAvgText(doc.get("avgText")); }
    if (doc.get("avgSent") != null) {this.setAvgSent(doc.get("avgSent")); }
    if (doc.get("totRetweet") != null) {this.setTotRetweet(doc.get("totRetweet")); }
    if (doc.get("totFav") != null) {this.setTotFav(doc.get("totFav")); }
    
	//} catch (ParseException e) {System.out.println("error: "+e); }
}

}
