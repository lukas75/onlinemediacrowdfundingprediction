package at.draschkowitz.cfduktiction.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.SortedNumericDocValuesField;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexOptions;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import at.draschkowitz.cfduktiction.model.Reward;

//@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.WRAPPER_OBJECT, property="@class")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"photo",
"name",
"blurb",
"goal",
"pledged",
"state",
"slug",
"disable_communication",
"country",
"currency",
"currency_symbol",
"currency_trailing_code",
"deadline",
"state_changed_at",
"created_at",
"launched_at",
"staff_pick",
"backers_count",
"static_usd_rate",
"usd_pledged",
"creator",
"location",
"category",
"profile",
"spotlight",
"urls",
"updated_at",
"video",
"comments_count",
"updates_count",
"rewards",
"items",
"livestreams"
})

//id,photo,name,blurb,goal,pledged,state,slug,disable_communication,country,currency,currency_symbol,currency_trailing_code,deadline,state_changed_at
//,created_at,launched_at,staff_pick,backers_count,static_usd_rate,usd_pledged,creator,location,category,profile,spotlight,urls,source_url,friends,
//is_starred,is_backing,permissions


/**
 * Model Class for Kickstarter Project Objects 
 * Jackson Annotated to be sent over REST interface as JSON
 *
 */
public class KickstarterProject extends Project implements IndexingObject {

@JsonProperty("id")
private Integer id = 0;
@JsonProperty("name")
private String name = "";
@JsonProperty("blurb")
private String blurb = "";
@JsonProperty("goal")
private Double goal = 0.0;
@JsonProperty("pledged")
private Double pledged = 0.0;
@JsonProperty("state")
private String state = "";
@JsonProperty("slug")
private String slug = "";
@JsonProperty("disable_communication")
private Boolean disableCommunication = false;
@JsonProperty("country")
private String country = "";
@JsonProperty("currency")
private String currency = "";
@JsonProperty("currency_symbol")
private String currencySymbol = "";
@JsonProperty("currency_trailing_code")
private Boolean currencyTrailingCode = false;
@JsonProperty("deadline")
private Date deadline= null;
@JsonProperty("state_changed_at")
private Date stateChangedAt = null;
@JsonProperty("created_at")
private Date createdAt = null;
@JsonProperty("launched_at")
private Date launchedAt = null ;
@JsonProperty("staff_pick")
private Boolean staffPick = false;
@JsonProperty("backers_count")
private Integer backersCount = 0;
@JsonProperty("static_usd_rate")
private Double staticUsdRate = 0.0;
@JsonProperty("usd_pledged")
private String usdPledged = "";
@JsonProperty("spotlight")
private Boolean spotlight = false;
@JsonProperty("updated_at")
private Date updatedAt = null;
@JsonProperty("video")
private Object video = null;
@JsonProperty("comments_count")
private Integer commentsCount = 0;
@JsonProperty("updates_count")
private Integer updatesCount = 0 ;
@JsonProperty("rewards")
private List<Reward> rewards = null;
@JsonProperty("items")
private List<Object> items = null;
@JsonProperty("livestreams")
private List<Object> livestreams = null;
@JsonIgnore
private String articleLink="";
@JsonIgnore
private String articlePage="";
@JsonIgnore
private String description=""; 
@JsonIgnore
private String challenges="";

//pledged amount to date --this value gets updated
@JsonIgnore
private Double pledged_initial = -1.0;

/**
* No args constructor for use in serialization
* 
*/
public KickstarterProject() {
	super();
}

/**
* 
* @param state
* @param commentsCount
* @param backersCount
* @param currency
* @param id
* @param usdPledged
* @param pledged
* @param launchedAt
* @param createdAt
* @param name
* @param staticUsdRate
* @param video
* @param currencyTrailingCode
* @param disableCommunication
* @param updatesCount
* @param rewards
* @param livestreams
* @param blurb
* @param staffPick
* @param goal
* @param country
* @param updatedAt
* @param items
* @param currencySymbol
* @param slug
* @param deadline
* @param spotlight
* @param stateChangedAt
*/
public KickstarterProject(Integer id,  String name, String blurb, Double goal, Double pledged, String state, String slug, 
		Boolean disableCommunication, String country, String currency, String currencySymbol, Boolean currencyTrailingCode, Long deadline, 
		Long stateChangedAt, Long createdAt, Long launchedAt, Boolean staffPick, Integer backersCount, Double staticUsdRate, 
		String usdPledged, Boolean spotlight, Long updatedAt, Object video, Integer commentsCount, Integer updatesCount, List<Reward> rewards, List<Object> items, List<Object> livestreams) {
super();
this.id = id;
this.name = name;
this.blurb = blurb;
this.goal = goal;
this.pledged = pledged;
this.state = state;
this.slug = slug;
this.disableCommunication = disableCommunication;
this.country = country;
this.currency = currency;
this.currencySymbol = currencySymbol;
this.currencyTrailingCode = currencyTrailingCode;
this.deadline = new Date(deadline); 
this.stateChangedAt = new Date(stateChangedAt);
this.createdAt = new Date(createdAt); 
this.launchedAt = new Date(launchedAt);
this.staffPick = staffPick;
this.backersCount = backersCount;
this.staticUsdRate = staticUsdRate;
this.usdPledged = usdPledged;
this.spotlight = spotlight;
this.updatedAt = new Date(updatedAt);
this.video = video;
this.commentsCount = commentsCount;
this.updatesCount = updatesCount;
this.rewards = rewards;
this.items = items;
this.livestreams = livestreams;
this.description = ""; 
}

public KickstarterProject(Integer id,  String name, String blurb, Double goal, Double pledged, String state, String slug, 
		Boolean disableCommunication, String country, String currency, String currencySymbol, Boolean currencyTrailingCode, Long deadline, 
		Long stateChangedAt, Long createdAt, Integer launchedAt, Boolean staffPick, Integer backersCount, Double staticUsdRate, 
		String usdPledged, Boolean spotlight, Long updatedAt, Integer commentsCount, Integer updatesCount) {
super();
this.id = id;
this.name = name;
this.blurb = blurb;
this.goal = goal;
this.pledged = pledged;
this.state = state;
this.slug = slug;
this.disableCommunication = disableCommunication;
this.country = country;
this.currency = currency;
this.currencySymbol = currencySymbol;
this.currencyTrailingCode = currencyTrailingCode;
this.deadline = new Date(deadline); 
this.stateChangedAt = new Date(stateChangedAt);
this.createdAt = new Date(createdAt); 
this.launchedAt = new Date(launchedAt);
this.staffPick = staffPick;
this.backersCount = backersCount;
this.staticUsdRate = staticUsdRate;
this.usdPledged = usdPledged;
this.spotlight = spotlight;
this.updatedAt = new Date(updatedAt);
this.video = null;
this.commentsCount = commentsCount;
this.updatesCount = updatesCount;
this.rewards = null;
this.items = null;
this.livestreams = null;
}

@JsonProperty("id")
public Integer getId() {
return id;
}

@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}

@JsonProperty("name")
public String getName() {
return name;
}

@JsonProperty("name")
public void setName(String name) {
this.name = name;
}

@JsonProperty("blurb")
public String getBlurb() {
return blurb;
}

@JsonProperty("blurb")
public void setBlurb(String blurb) {
this.blurb = blurb;
}

@JsonProperty("goal")
public Double getGoal() {
return goal;
}

@JsonProperty("goal")
public void setGoal(Double goal) {
this.goal = goal;
}

@JsonProperty("pledged")
public Double getPledged() {
return pledged;
}

@JsonProperty("pledged")
public void setPledged(Double pledged) {
this.pledged = pledged;
}

@JsonProperty("state")
public String getState() {
return state;
}

@JsonProperty("state")
public void setState(String state) {
this.state = state;
}

@JsonProperty("slug")
public String getSlug() {
return slug;
}

@JsonProperty("slug")
public void setSlug(String slug) {
this.slug = slug;
}

@JsonProperty("disable_communication")
public Boolean getDisableCommunication() {
return disableCommunication;
}

@JsonProperty("disable_communication")
public void setDisableCommunication(Boolean disableCommunication) {
this.disableCommunication = disableCommunication;
}

@JsonProperty("country")
public String getCountry() {
return country;
}

@JsonProperty("country")
public void setCountry(String country) {
this.country = country;
}

@JsonProperty("currency")
public String getCurrency() {
return currency;
}

@JsonProperty("currency")
public void setCurrency(String currency) {
this.currency = currency;
}

@JsonProperty("currency_symbol")
public String getCurrencySymbol() {
return currencySymbol;
}

@JsonProperty("currency_symbol")
public void setCurrencySymbol(String currencySymbol) {
this.currencySymbol = currencySymbol;
}

@JsonProperty("currency_trailing_code")
public Boolean getCurrencyTrailingCode() {
return currencyTrailingCode;
}

@JsonProperty("currency_trailing_code")
public void setCurrencyTrailingCode(Boolean currencyTrailingCode) {
this.currencyTrailingCode = currencyTrailingCode;
}

@JsonProperty("deadline")
public Date getDeadline() {
return deadline;
}

@JsonProperty("deadline")
public void setDeadline(Date deadline) {
this.deadline = deadline;
}

@JsonProperty("state_changed_at")
public Date getStateChangedAt() {
return stateChangedAt;
}

@JsonProperty("state_changed_at")
public void setStateChangedAt(Date stateChangedAt) {
this.stateChangedAt = stateChangedAt;
}

@JsonProperty("created_at")
public Date getCreatedAt() {
return createdAt;
}

@JsonProperty("created_at")
public void setCreatedAt(Date createdAt) {
this.createdAt = createdAt;
}

@JsonProperty("launched_at")
public Date getLaunchedAt() {
return launchedAt;
}

@JsonProperty("launched_at")
public void setLaunchedAt(Date launchedAt) {
this.launchedAt = launchedAt;
}

@JsonProperty("staff_pick")
public Boolean getStaffPick() {
return staffPick;
}

@JsonProperty("staff_pick")
public void setStaffPick(Boolean staffPick) {
this.staffPick = staffPick;
}

@JsonProperty("backers_count")
public Integer getBackersCount() {
return backersCount;
}

@JsonProperty("backers_count")
public void setBackersCount(Integer backersCount) {
this.backersCount = backersCount;
}

@JsonProperty("static_usd_rate")
public Double getStaticUsdRate() {
return staticUsdRate;
}

@JsonProperty("static_usd_rate")
public void setStaticUsdRate(Double staticUsdRate) {
this.staticUsdRate = staticUsdRate;
}

@JsonProperty("usd_pledged")
public String getUsdPledged() {
return usdPledged;
}

@JsonProperty("usd_pledged")
public void setUsdPledged(String usdPledged) {
this.usdPledged = usdPledged;
}

@JsonProperty("spotlight")
public Boolean getSpotlight() {
return spotlight;
}

@JsonProperty("spotlight")
public void setSpotlight(Boolean spotlight) {
this.spotlight = spotlight;
}

@JsonProperty("updated_at")
public Date getUpdatedAt() {
return updatedAt;
}

@JsonProperty("updated_at")
public void setUpdatedAt(Date updatedAt) {
this.updatedAt = updatedAt;
}

@JsonProperty("video")
public Object getVideo() {
return video;
}

@JsonProperty("video")
public void setVideo(Object video) {
this.video = video;
}

@JsonProperty("comments_count")
public Integer getCommentsCount() {
return commentsCount;
}

@JsonProperty("comments_count")
public void setCommentsCount(Integer commentsCount) {
this.commentsCount = commentsCount;
}

@JsonProperty("updates_count")
public Integer getUpdatesCount() {
return updatesCount;
}

@JsonProperty("updates_count")
public void setUpdatesCount(Integer updatesCount) {
this.updatesCount = updatesCount;
}

@JsonProperty("rewards")
public List<Reward> getRewards() {
return rewards;
}

@JsonProperty("rewards")
public void setRewards(List<Reward> rewards) {
this.rewards = rewards;
}

@JsonProperty("items")
public List<Object> getItems() {
return items;
}

@JsonProperty("items")
public void setItems(List<Object> items) {
this.items = items;
}

@JsonProperty("livestreams")
public List<Object> getLivestreams() {
return livestreams;
}

@JsonProperty("livestreams")
public void setLivestreams(List<Object> livestreams) {
this.livestreams = livestreams;
}
@JsonIgnore
public String getArticleLink() {
	return this.articleLink;
}
@JsonIgnore
public void setArticleLink(String articleLink) {
	this.articleLink = articleLink;
}
@JsonIgnore
public String getDescription() {
	return description;
}
@JsonIgnore
public void setDescription(String description) {
	this.description = description;
}
@JsonIgnore
@JsonProperty("challenges")
public String getChallenges() {
	return challenges;
}
@JsonIgnore
@JsonProperty("challenges")
public void setChallenges(String challenges) {
	this.challenges = challenges;
}
@JsonIgnore
public Double getPledged_initial() {
	return pledged_initial;
}
@JsonIgnore
public void setPledged_initial(Double pledged_initial) {
	this.pledged_initial = pledged_initial;
}
@JsonIgnore
public String getArticlePage() {
	return articlePage;
}
@JsonIgnore
public void setArticlePage(String articlePage) {
	this.articlePage = articlePage;
}

@Override
@JsonIgnore
public String toString() {
	return "KickstarterProject [id=" + id + ", name=" + name + ", blurb=" + blurb + ", goal=" + goal + ", pledged="
			+ pledged + ", state=" + state + ", slug=" + slug + ", disableCommunication=" + disableCommunication
			+ ", country=" + country + ", currency=" + currency + ", currencySymbol=" + currencySymbol
			+ ", currencyTrailingCode=" + currencyTrailingCode + ", deadline=" + deadline + ", stateChangedAt="
			+ stateChangedAt + ", createdAt=" + createdAt + ", launchedAt=" + launchedAt + ", staffPick=" + staffPick
			+ ", backersCount=" + backersCount + ", staticUsdRate=" + staticUsdRate + ", usdPledged=" + usdPledged
			+ ", spotlight=" + spotlight + ", updatedAt=" + updatedAt + ", video=" + video + ", commentsCount="
			+ commentsCount + ", updatesCount=" + updatesCount + ", rewards=" + rewards + ", items=" + items
			+ ", livestreams=" + livestreams + ", description=" + description + "challenges"+challenges+"articleLink"+articleLink+"]";
}

@Override
@JsonIgnore
public Document getDocument() {
	Document doc = new Document();
    FieldType fieldType = new FieldType();
    fieldType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS_AND_OFFSETS);
    fieldType.setStored(true);
    fieldType.setStoreTermVectors(true);
    fieldType.setTokenized(true);
    Field contentField = new Field("contents", this.getBlurb(), fieldType);
    Field contentField1 = new Field("description", this.getDescription(), fieldType);
    Field contentField2 = new Field("title", this.getName(), fieldType);
    doc.add(contentField);doc.add(contentField1);doc.add(contentField2); 
    doc.add(new StringField("country", this.getCountry(), Field.Store.YES));
    doc.add(new StringField("challenges", this.getChallenges(), Field.Store.YES));
    doc.add(new StringField("currency", this.getCurrency(), Field.Store.YES));
    doc.add(new StringField("slug", this.getSlug(), Field.Store.YES));
    doc.add(new StringField("state", this.getState(), Field.Store.YES));
    doc.add(new StringField("backersCount", this.getBackersCount().toString(), Field.Store.YES));
    doc.add(new StringField("usdplegded", this.getUsdPledged(), Field.Store.YES));
    doc.add(new StringField("comments", this.getCommentsCount().toString(), Field.Store.YES));
    doc.add(new StringField("createdat", String.valueOf(this.getCreatedAt().getTime()), Field.Store.YES));
    doc.add(new StringField("updatesCount", this.getUpdatesCount().toString(), Field.Store.YES));
    doc.add(new StringField("deadline", String.valueOf(this.getDeadline().getTime()), Field.Store.YES));
    doc.add(new StringField("goal", this.getGoal().toString(), Field.Store.YES));
    doc.add(new StringField("launchedAt", String.valueOf(this.getLaunchedAt().getTime()), Field.Store.YES));
    doc.add(new StringField("pledged", String.valueOf(this.getPledged()), Field.Store.YES));
    doc.add(new StringField("staticRate", this.getStaticUsdRate().toString(), Field.Store.YES));
    doc.add(new StringField("staffpick", this.getStaffPick().toString(), Field.Store.YES));
    doc.add(new StringField("id", String.valueOf(this.getId()), Field.Store.YES));
    doc.add(new SortedNumericDocValuesField("iscore", this.getScore()));
    doc.add(new SortedNumericDocValuesField("isimscore", (int) this.getSimScore()));
    doc.add(new SortedNumericDocValuesField("ipopscore", (int) this.getPopScore()));
    doc.add(new StoredField("score", this.getScore()));
    doc.add(new StoredField("simscore", this.getSimScore()));
    doc.add(new StoredField("popscore", this.getPopScore()));
    doc.add(new SortedNumericDocValuesField("ipledged", this.getPledged().intValue()));
    doc.add(new SortedNumericDocValuesField("igoal", (int) this.getGoal().intValue()));
    doc.add(new StringField("articleLink", this.getArticleLink(), Field.Store.YES));
    doc.add(new StringField("articlePage", this.getArticlePage(), Field.Store.YES));
    doc.add(new StringField("pledgedInitial", String.valueOf(this.getPledged()), Field.Store.YES));
 
    doc.add(new StringField("newsstat", String.valueOf(this.getNewsStat()), Field.Store.YES));
    doc.add(new StringField("twitterstat", String.valueOf(this.getTwitterStat()), Field.Store.YES));
    
    doc.add(new StringField("avgText", String.valueOf(this.getAvgText()), Field.Store.YES));
    doc.add(new StringField("avgSent", String.valueOf(this.getAvgSent()), Field.Store.YES));
    doc.add(new StringField("totRetweet", String.valueOf(this.getTotRetweet()), Field.Store.YES));
    doc.add(new StringField("totFav", String.valueOf(this.getTotFav()), Field.Store.YES));
    
	return doc; 
}

//constructor to generate Object from Lucene Document 
public KickstarterProject(Document doc) {
	this.blurb = doc.get("contents");
	this.description = doc.get("description");
	this.name = doc.get("title");
    this.country = doc.get("country");
    this.challenges = doc.get("challenges");
    this.currency = doc.get("currency");
    this.slug = doc.get("slug");
    this.state = doc.get("state");
    if (doc.get("backersCount")!= null) {this.backersCount = Integer.valueOf(doc.get("backersCount"));}
    if (doc.get("comments") != null) {this.commentsCount = Integer.valueOf(doc.get("comments"));}
    this.createdAt = null; //doc.get("createdat")!= null ? new Date(Long.parseLong(doc.get("createdat"))*1000) : null;
    if (doc.get("updatesCount")!= null) { this.updatesCount = Integer.valueOf(doc.get("updatesCount"));}
    //this.deadline = doc.get("deadline")  != null ? new SimpleDateFormat("yyyy-mm-dd").parse("deadline") : null;
    if (doc.get("goal") != null) {this.goal = Double.valueOf(doc.get("goal"));}
    this.launchedAt = doc.get("launchedAt")!= null ? new Date(Long.parseLong(doc.get("launchedAt"))*1000) : null;
    DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.US); 
    Date date = null; 
    if (doc.get("deadline") != null) {try {
		date = (Date)formatter.parse(doc.get("deadline"));
	} catch (ParseException e) {}}
    if (date !=null) {this.deadline = new Date(date.getTime()*1000);  }
//    else {this.deadline = doc.get("deadline")!= null ? new Date(Long.parseLong(doc.get("deadline"))*1000) : null;}
    if (doc.get("pledged")!= null) {this.pledged = Double.valueOf(doc.get("pledged"));}
    if (doc.get("staticRate")!= null) { this.staticUsdRate = Double.valueOf(doc.get("staticRate"));}
    if (doc.get("staffpick")!= null) { this.staffPick = Boolean.valueOf(doc.get("staffpick"));}
    this.id  = doc.get("id") != null ? Integer.valueOf(doc.get("id")) : null;
    this.usdPledged = doc.get("usdPledged");
    this.updatedAt = doc.get("updatedAt")!= null ? new Date(Long.parseLong(doc.get("updatedAt"))*1000) : null;
    this.disableCommunication = null;
    this.currencySymbol = null;
    this.currencyTrailingCode = null;
    this.stateChangedAt = null;
    this.spotlight = null; 
    this.rewards = null;  this.items = null;  this.livestreams = null; this.video = null;
    this.articleLink = doc.get("articleLink"); 
    this.articlePage = doc.get("articlePage"); 
    if (doc.get("score") != null) {this.setScore(Integer.parseInt(doc.get("score")));} else {this.setScore(0); }
    if (doc.get("simscore") != null) { this.setSimScore(Double.parseDouble(doc.get("simscore"))); } else {this.setSimScore(0);}
    if (doc.get("popscore") != null) { this.setPopScore(Double.parseDouble(doc.get("popscore")));} else {this.setPopScore(0);}
    super.setConceptname(this.name);
    super.setPlatform("Kickstarter");
    if (doc.get("pledgedInitial") != null) {this.pledged_initial = Double.parseDouble(doc.get("pledgedInitial"));} else {this.setPledged_initial(0.0);}
    
    if (doc.get("twitterstat") != null) {this.setTwitterStat(Integer.parseInt(doc.get("twitterstat"))); }
    if (doc.get("newsstat") != null) {this.setNewsStat(Integer.parseInt(doc.get("newsstat"))); }
    
    if (doc.get("avgText") != null) {this.setAvgText(doc.get("avgText")); }
    if (doc.get("avgSent") != null) {this.setAvgSent(doc.get("avgSent")); }
    if (doc.get("totRetweet") != null) {this.setTotRetweet(doc.get("totRetweet")); }
    if (doc.get("totFav") != null) {this.setTotFav(doc.get("totFav")); }
}

}
