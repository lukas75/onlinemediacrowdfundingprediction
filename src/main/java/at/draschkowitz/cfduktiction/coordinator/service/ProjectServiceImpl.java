package at.draschkowitz.cfduktiction.coordinator.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.digitalpebble.stormcrawler.ConfigurableTopology;

import at.draschkowitz.cfduktiction.aggregator.PopularityPrediction;
import at.draschkowitz.cfduktiction.aggregator.SimilarityPrediction;
import at.draschkowitz.cfduktiction.crawler.KickstarterReader;
import at.draschkowitz.cfduktiction.crawler.StormCrawler;
import at.draschkowitz.cfduktiction.indexer.Indexer;
import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;


/**
 * 
 * @author lukasdraschkowitz
 *provides service to rest controller and implements business logic and workflow 
 *e.g. set up and manage project lists 
 *(list for GUI presentation, training set list, projects list of projects to be scored)
 */
@Service("projectService")
public class ProjectServiceImpl implements ProjectService{
		
	//projects for presentation in GUI 
	private static List<Project> projects;
	//training set machine learning component uses for training the model
	private static List<Project> trainingProjects;
	//projects to be scored
	public static List<Project> computeProjects;
	Indexer in;
	
	static{
		computeProjects = new ArrayList<Project>();
	}
	
	public ProjectServiceImpl() { 
		this.in = new Indexer(); 
		in.setIndex("project");
		findAllProjects();
		createTrainingSet();
		
		System.out.println("Training Set: "+trainingProjects.size());
		System.out.println("Compute Set: "+computeProjects.size());
		System.out.println("Visible Set: "+projects.size());
		
		//endless loop retrieving projects 
//		retrieveProjects(); 
	} 
	
	//update projects once in a while 
    @Scheduled(cron = "0 0 12 ? * 1 *") //12 a.m on every first day of the week
    //cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of week] [Year]")
    public void updateProjects() {
		Runnable myrunnable = new Runnable() {
		    public void run() {
				KickstarterReader bwc = new KickstarterReader();
				bwc.updateProjectsNew();		    }
		}; 
		new Thread(myrunnable).start();//Call it when you need to run the function

    }
    
	//start crawling news sites once a month
    @Scheduled(cron = "0 0 12 1 * ? *") //12 a.m on every first day of the week
    public void updateNewsSites() {
		Runnable myrunnable = new Runnable() {
		    public void run() {
		    	try {
		    	ConfigurableTopology.start(new StormCrawler(), new String[]{"-conf crawlerconf.yaml","-local"} );
		    	} catch (Exception e) {e.printStackTrace();}
		    }
		}; 
		new Thread(myrunnable).start(); //Call it when you need to run the function
    }
	

	
	//all docs that are scored by the system
	private void createTrainingSet() {
		trainingProjects = in.scoreDocs(); 
	}
	
	@Override
	public List<Project> getTrainingProjects() {
		return trainingProjects; 
	}

	//to frequently search for new projects
		public void retrieveProjects() {
			System.out.println("getTrainingProjects"); 
			Runnable myrunnable = new Runnable() {
			    public void run() {
			    	while (true) {
				    try {Thread.sleep(100000);} catch (InterruptedException e) {e.printStackTrace();} 
					KickstarterReader bwc = new KickstarterReader();
			        bwc.getPageLinks("https://www.kickstarter.com/discover/advanced?google_chrome_workaround&woe_id=0&sort=newest&seed=2488368&page=1",1); 
			        bwc.retrieveProjects(computeProjects); //function stores retrieved projects in computeProjects
			    	}
			    }
			}; 
			new Thread(myrunnable).start();//Call it when you need to run the function
		}
	
	
	//return list of projects to present
	@Override
	public List<Project> retSet() {
		return projects; 
	}

	//create and return list of projects to present
	public List<Project> findAllProjects() {
		Indexer in = new Indexer(); 
		in.setIndex("project");
		projects = in.searchnewDocs();
		return projects;
	}
	
	//retrieve presentation project by id 
	public Project findById(long id) {
		for(Project project : projects){
			if(project.getPid() == id){
				return project;
			}
		}
		return null;
	}
	
	//retrieve presentation project by name 
	public Project findByName(String name) {
		for(Project project : projects){
			if(project.getConceptname().equalsIgnoreCase(name)){
				return project;
			}
		}
		return null;
	}
	
	//has to be implemented differently when running getTrainingsets at the same time ! 
	@Override
	public void indexProject(List<KickstarterProject> projects) {
		for (KickstarterProject p : projects) {
			//if (p.getScore() > projects.get(49).getScore()) {
				projects.add(p); 
			//}
		}
		in.index(projects);
	}
	
	//create project send from GUI and prepare for scoring
	public void saveProject(Project project) {
		//project.setPid((int) counter.incrementAndGet());
		double simScore = 0; 
		simScore = SimilarityPrediction.computeSimilarity(project.getConceptname()); 
		double popScore = 0; 
		double[] pop = PopularityPrediction.computePopularity(project.getConceptname(),project.getConceptname());
		popScore = pop[0]; 
		project.setSimScore(simScore);
		project.setPopScore(popScore);
		System.out.println("sim: "+ simScore + "pop: "+ popScore); 
		project.setTwitterStat((int)pop[1]);
		project.setNewsStat((int)pop[2]);
		project.setAvgText(Double.toString(pop[3]));
		project.setAvgSent(Double.toString(pop[4]));
		project.setTotRetweet(Double.toString(pop[5]));
		project.setTotFav(Double.toString(pop[6]));
		
		computeProjects.add(project); //these projects will be scored by machine learning component
	}

	public void updateProject(Project project) {
		int index = projects.indexOf(project);
		projects.set(index, project);
	}

	public void deleteProjectById(long id) {
		
		for (Iterator<Project> iterator = projects.iterator(); iterator.hasNext(); ) {
			Project project = iterator.next();
		    if (project.getPid() == id) {
		        iterator.remove();
		    }
		}
	}

	public boolean isProjectExist(Project project) {
		return findByName(project.getConceptname())!=null;
	}
	
	public void deleteAllProjects(){
		projects.clear();
	}

	@Override
	public List<Project> getScoreSet() {
		return computeProjects; 
	}
	

}
