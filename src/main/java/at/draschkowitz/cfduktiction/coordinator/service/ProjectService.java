package at.draschkowitz.cfduktiction.coordinator.service;

import java.util.List;

import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;


public interface ProjectService {
	
	Project findById(long id);
	
	Project findByName(String name);
	
	void saveProject(Project user);
	
	void updateProject(Project user);
	
	void deleteProjectById(long id);

	List<Project> findAllProjects();  
	
	void deleteAllProjects();
	
	public boolean isProjectExist(Project user);

	List<Project> getTrainingProjects();

	void indexProject(List<KickstarterProject> project);

	List<Project> getScoreSet();

	List<Project> retSet();
	
}
