package at.draschkowitz.cfduktiction.coordinator.controller;
 
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import at.draschkowitz.cfduktiction.coordinator.service.ProjectService;
import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;
 
/* 
 * RESTController 
 * provides interfaces to GUI and Python Machine Learning Component
 * 
 * index at server:port/OnlineMediaCrowdfundingPrediction/
 * http://localhost:8080/OnlineMediaCrowdfundingPrediction/
 * */


@RestController
public class HelloWorldRestController {

 
    @Autowired
    ProjectService projectService;  //Service which will do all data retrieval/manipulation work
    
    //------------------receive projects with score from python application --------------------------------------------------------
    
    @RequestMapping(value = "/scored/", method = RequestMethod.POST)
    public ResponseEntity<Void> scoredProject(@RequestBody List<KickstarterProject> project,    UriComponentsBuilder ucBuilder) {
        System.out.println("received "+project.size()); 
        projectService.getScoreSet().clear();
    	projectService.indexProject(project);
        return new ResponseEntity<Void>(HttpStatus.CREATED);
    }
    
    //-------------------Retrieve training Projects--------------------------------------------------------
    @RequestMapping(value = "/train/", method = RequestMethod.GET)
    public ResponseEntity<List<Project>> getTrainingProjects() {
        List<Project> projects = projectService.getTrainingProjects();
        System.out.println("trainset sending "+projects.size()); 
        if(projects ==null || projects.isEmpty()){
            return new ResponseEntity<List<Project>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        //Collections.sort(projects);
        return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
    }
    
    //-------------------Retrieve  Projects to score--------------------------------------------------------
    @RequestMapping(value = "/toscore/", method = RequestMethod.GET)
    public ResponseEntity<List<Project>> getToscore() {
        List<Project> projects = projectService.getScoreSet();
        if(projects ==null || projects.isEmpty()){
            return new ResponseEntity<List<Project>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        System.out.println("Projects to score: "+projects.size());
        return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
    }
    
  //-------------------Retrieve top 50 Projects--------------------------------------------------------
    @RequestMapping(value = "/project/", method = RequestMethod.GET)
    public ResponseEntity<List<Project>> listAllProjects() {
//        List<Project> projects = projectService.findAllProjects();
        List<Project> projects = projectService.retSet();
        if(projects.isEmpty()){
            return new ResponseEntity<List<Project>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        Collections.sort(projects);
        return new ResponseEntity<List<Project>>(projects, HttpStatus.OK);
    }
 
    //-------------------Retrieve Single Project--------------------------------------------------------
    
    @RequestMapping(value = "/project/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Project> getProject(@PathVariable("id") long id) {
        System.out.println("Fetching Project with id " + id);
        Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("Project with id " + id + " not found");
            return new ResponseEntity<Project>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Project>(project, HttpStatus.OK);
    }
    
    //-------------------Create a Project--------------------------------------------------------
    
    @RequestMapping(value = "/project/", method = RequestMethod.POST)
    public ResponseEntity<Void> createProject(@RequestBody Project project,    UriComponentsBuilder ucBuilder) {
        System.out.println("Creating Project " + project.getConceptname()+ project.getPlatform());
        
 
        if (projectService.isProjectExist(project)) {
            System.out.println("A Project with name " + project.getConceptname() + " already exist");
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
 
        projectService.saveProject(project);
 
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/project/{id}").buildAndExpand(project.getPid()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    //------------------- Delete a Project --------------------------------------------------------
    
    @RequestMapping(value = "/project/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Project> deleteProject(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Project with id " + id);
 
        Project project = projectService.findById(id);
        if (project == null) {
            System.out.println("Unable to delete. Project with id " + id + " not found");
            return new ResponseEntity<Project>(HttpStatus.NOT_FOUND);
        }
 
        projectService.deleteProjectById(id);
        return new ResponseEntity<Project>(HttpStatus.NO_CONTENT);
    }
    
    //------------------- Delete All Projects --------------------------------------------------------
    
    @RequestMapping(value = "/project/", method = RequestMethod.DELETE)
    public ResponseEntity<Project> deleteAllProjects() {
        System.out.println("Deleting All Projects");
 
        projectService.deleteAllProjects();
        return new ResponseEntity<Project>(HttpStatus.NO_CONTENT);
    }
 
}