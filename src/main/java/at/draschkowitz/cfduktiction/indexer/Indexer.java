package at.draschkowitz.cfduktiction.indexer;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.Term;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MatchAllDocsQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField.Type;
import org.apache.lucene.search.SortedNumericSortField;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.search.DocValuesRangeQuery;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import at.draschkowitz.cfduktiction.model.IndexingObject;
import at.draschkowitz.cfduktiction.model.KickstarterProject;
import at.draschkowitz.cfduktiction.model.Project;


/**
 * provides interface to Lucene - e.g. methods to store and retrieve from indices
 * creates and manages Lucene indices for online news and crowdfunding Projects
 * Tweets are not stored in the system
 * started on this tutorialhttp://computergodzilla.blogspot.co.at/2015/01/calculate-cosine-similarity-using.html
 */
public class Indexer {
	


    private File indexDirectory;
    //private String root = "/Users/lukasdraschkowitz/git/cf/Spring4MVCAngularJSExample/"; 
    private String root; 
    private String INDEX_DIRECTORY = ""; 
    
    //read location of lucene index from file
	public Indexer() {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("index.txt").getFile());
        BufferedReader b;
		try {
			b = new BufferedReader(new FileReader(file));
			root = b.readLine();
			b.close();
        } catch (Exception e) {System.out.println(e);}
	}
    
    /**
     * set the index to work on
     * currently either "project" for crowd funding index
     * or "news" for online news 
     */
	public void setIndex(String indexType) {
	    
    	if (indexType.equalsIgnoreCase("project")) {INDEX_DIRECTORY = root+"/projects"; }
    	else if (indexType.equalsIgnoreCase("facebook")) {INDEX_DIRECTORY = root+"/facebook"; }
    	else if (indexType.equalsIgnoreCase("twitter")) {INDEX_DIRECTORY = root+"/twitter"; }
    	else {INDEX_DIRECTORY = root+"/news";}
    	System.out.println(INDEX_DIRECTORY); 
        this.indexDirectory = new File(INDEX_DIRECTORY);
	}
    
    /**
     * compute similarity between querystring and projects, specifically string of field "fields"
     * used in project similarity computation
     */
    public double stringSimilarity(String[] fields, String querystr) {
		double score = 0; 
    	try {   	
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
	    IndexReader reader = DirectoryReader.open(dir);
	    IndexSearcher searcher = new IndexSearcher(reader);
        MoreLikeThis mlt = new MoreLikeThis(reader);
	    mlt.setMinDocFreq(0);
	    mlt.setMinTermFreq(0);
	    mlt.setMinWordLen(0);
	    mlt.setAnalyzer(new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET));
	    System.out.println("querystr: "+querystr);
	    mlt.setFieldNames(fields);
	    
    					    Reader sReader = new StringReader(querystr);
    					    Query likeQuery = mlt.like(fields[0], sReader);

    	        		    TopDocs results = searcher.search(likeQuery, 5);
    	        		    ScoreDoc[] hits1 = results.scoreDocs; 
    	        		    //	Code to display the results of search
    	        		    for (int j=0;j<hits1.length;j++) {
    	        		      if (j > 0 ) {score= (score + (double)hits1[j].score); }
    	        		    }
    		    reader.close();
    		} catch(Exception e) {System.out.println(e.getMessage());}
	return score;  //score/10
}
	
    /**
     * search for project in index and compute similarity to other projects
     * compute similarity to other projects by comparing strings of field "fields"
     * used in news computation 
     */
	public double similarity(String[] fields, String querystr) {
			double score = 0; 
        	try {   	
        			Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));           
            
            		Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
        			Query q = new QueryParser(fields[0], analyzer).parse(querystr);
      
        			// Searching code
        		    IndexReader reader = DirectoryReader.open(dir);
        		    IndexSearcher searcher = new IndexSearcher(reader);
        		    TopScoreDocCollector collector = TopScoreDocCollector.create(1); //create(hitsPerPage);
        		    searcher.search(q, collector);
        		    ScoreDoc[] hits = collector.topDocs().scoreDocs;
        		    
        		    int docId = hits[0].doc;
   		    
        		    MoreLikeThis mlt = new MoreLikeThis(reader);  
        		    mlt.setMinDocFreq(0);
        		    mlt.setMinTermFreq(0);
        		    mlt.setAnalyzer(new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET));
        		    mlt.setFieldNames(fields);
        	        		    Query likeQuery = mlt.like(docId);
        	        		    TopDocs results = searcher.search(likeQuery, 5);
        	        		    ScoreDoc[] hits1 = results.scoreDocs; 
        	        		    //	Code to display the results of search
        	        		    System.out.println("Found " + hits.length + " hits.");
        	        		    for (int j=0;j<hits1.length;j++) {
        	        		      if (j > 0 ) {score= (score + (double)hits1[j].score); }
        	        		    }
        		    // reader can only be closed when there is no need to access the documents any more
        		    reader.close();
        		} catch(Exception e) {System.out.println(e.getMessage());}
    	return score; 
    }
	
    /**
     * Method to index IndexingObject in Lucene
     */
    public synchronized void index(List<? extends IndexingObject> o) {
    	try {
    		System.out.println("indexing "+o.size());
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        
        IndexWriter writer = new IndexWriter(dir, iwc);
        for (IndexingObject f : o) {
            Document doc = f.getDocument(); 
			Term t = new Term("title", doc.get("title")); 
            writer.updateDocument(t , doc);
        }
        writer.commit(); 
        writer.close();
    		}
    		catch(Exception e) //CorruptIndexException, LockObtainFailedException, IOException
    		{e.printStackTrace();} 
    }
    
    /**
     * Method to index Lucene Documents in Lucene
     */
    public synchronized void index(Document doc) {
    	try {
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        IndexWriter writer = new IndexWriter(dir, iwc);
        Term t = new Term("title", doc.get("title")); 
        writer.updateDocument(t , doc);
        writer.commit(); 
        writer.close();
    	}
    		catch(Exception e) //CorruptIndexException, LockObtainFailedException, IOException
    		{System.out.println(e.getMessage());}
    }
    
    /**
     * checks if a project with a certain name exists in the Lucene index
     */
    public boolean pExists(String pTitle) {
    	boolean r = false; 
    	try {
	    	Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
	    	IndexReader reader = DirectoryReader.open(dir);
		    IndexSearcher searcher = new IndexSearcher(reader);
	    	TopDocs results = searcher.search(new TermQuery(new Term("title", pTitle)), 1);
	    	if (results.totalHits != 0){
	    		r =  true; System.out.println("already inside!!"); 
	    	}
    	} catch (IOException e) {System.out.println("IOException pexists"+e);}
    	return r; 
    }
    
    /**
     * returns all documents previously scored by the system
     */
    public List<Project> scoreDocs() {
    	List<Project> r = new ArrayList<Project>(); 
    	try {
	    	Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
	    	IndexReader reader = DirectoryReader.open(dir);
		    IndexSearcher searcher = new IndexSearcher(reader);
		    Query q = DocValuesRangeQuery.newLongRange("iscore", 0L, 100000L, false, true);
	    	TopDocs results = searcher.search(q, 10000);
 		   for (ScoreDoc scoreDoc : results.scoreDocs) {
 		      Document doc = searcher.doc(scoreDoc.doc);
 		      r.add(new KickstarterProject(doc)); 
 		   }
    	} catch (IOException e) {System.out.println("IOException pexists"+e);}
    	return r; 
    }
    
    /**
     * returns a document by searching for "querystr" in "fieldname" attribute
     */
    public Document search(String querystr, String fieldName) {
    	//Map<KickstarterProject, int>
    	//KickstarterProject a = new KickstarterProject(); 
    	Document d = null;
    	try {   	
        Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
    			Query q = new QueryParser(fieldName, analyzer).parse(querystr);
    		    IndexReader reader = DirectoryReader.open(dir);
    		    IndexSearcher searcher = new IndexSearcher(reader);
    		    System.out.println("Found files: "+ reader.getDocCount(fieldName));
    		    TopScoreDocCollector collector = TopScoreDocCollector.create(1); //create(hitsPerPage);
    		    searcher.search(q, collector);
    		    ScoreDoc[] hits = collector.topDocs().scoreDocs; 
    		    //	Code to display the results of search
    		    System.out.println("Found " + hits.length + " hits.");
    		    for (int i=0;i<hits.length;++i) {
    		      int docId = hits[i].doc;
    		      d = searcher.doc(docId);
    		    }
    		    reader.close();
    		}catch(Exception e){System.out.println(e.getMessage());}
    	return d;
    }
    

    
   
    /**
     * returns all documents in the index
     */
    public List<Document> searchForAllDocs() {
    	List<Document> p = new ArrayList<Document>(); 
    	try {
    		   Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));   
    		   IndexReader reader = DirectoryReader.open(dir);
    		   IndexSearcher searcher = new IndexSearcher(reader);
    		   Query q = new MatchAllDocsQuery();   		      		   
    		   //do the search
    		   ScoreDoc[] hits = searcher.search(q,300000).scoreDocs; 
    		   for (ScoreDoc scoreDoc : hits) {
    		      Document doc = searcher.doc(scoreDoc.doc);
    		      p.add(doc);
    		   }		  
    		   reader.close(); 
    			} catch (IOException e) {System.out.println("error: "+e.getMessage());}
    		System.out.println("size: " +p.size()); 
    		return p; 
    		}
    
    
    /**
     * returns top 50 projects from index ranked by score, that is generated by the system
     */
    public List<Project> searchnewDocs() {
    	List<Project> p = new ArrayList<Project>(); 
    	try {
    		   Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));   
    		   IndexReader reader = DirectoryReader.open(dir);
    		   IndexSearcher searcher = new IndexSearcher(reader);
    		   
    		   Query q = new MatchAllDocsQuery();   	   
    		   Sort sort = new Sort(new SortedNumericSortField("iscore",
                      Type.INT, true));
    		   TopDocs topDocs = searcher.search(q, 50, sort);	
 		     
    		   for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
    		      Document doc = searcher.doc(scoreDoc.doc);
    		      p.add(new KickstarterProject(doc)); 
    		   }
    		   reader.close(); 
    			} catch (IOException e) {System.out.println("error: "+e.getMessage());}
    		return p; 
    		}

    /**
     * retrieve all docs that have simscore asigned by the system - e.g. all docs analyzed 
     * but not necessarily scored
     * return Project Object
     */
    public List<Project> searchPreTrainingDocs() {
    	List<Project> p = new ArrayList<Project>(); 
    	try {
    		   Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));   
    		   IndexReader reader = DirectoryReader.open(dir);
    		   IndexSearcher searcher = new IndexSearcher(reader);
    		   
    		   Query q = new MatchAllDocsQuery();      		   
    		   Sort sort = new Sort(new SortedNumericSortField("isimscore", Type.INT, false));
    		   TopDocs topDocs = searcher.search(q, 100000, sort);
    		   
    		   for (ScoreDoc scoreDoc : topDocs.scoreDocs) {  			  
    		      Document doc = searcher.doc(scoreDoc.doc);
    		      if (Double.parseDouble(doc.get("simscore")) == 0.0) {
    		    	  p.add(new KickstarterProject(doc)); 
    		      }
    		   }

    		   reader.close(); 
    			} catch (IOException e) {System.out.println("error: "+e.getMessage());}
    		return p; 
    		}    
    
    /**
     * retrieve all docs that have simscore asigned by the system - e.g. all docs analyzed 
     * but not necessarily scored
     * return Lucene document
     */
    public List<Document> getPreTrainingDocuments() {
    	List<Document> p = new ArrayList<Document>(); 
    	try {
    		   Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));   
    		   IndexReader reader = DirectoryReader.open(dir);
    		   IndexSearcher searcher = new IndexSearcher(reader);
    		   
    		   Query q = new MatchAllDocsQuery();      		   
    		   Sort sort = new Sort(new SortedNumericSortField("isimscore", Type.INT, false));
    		   TopDocs topDocs = searcher.search(q, 100000, sort);
    		   
    		   for (ScoreDoc scoreDoc : topDocs.scoreDocs) {  			  
    		      Document doc = searcher.doc(scoreDoc.doc);
    		      if (Double.parseDouble(doc.get("simscore")) == 0.0) {
    		    	  p.add((doc)); 
    		      }
    		   }

    		   reader.close(); 
    			} catch (IOException e) {System.out.println("error: "+e.getMessage());}
    		return p; 
    		}   
    
    /**
     * helper function to generate csv file from index
     * writes all docs that have a score asigned by the system - e.g. all docs analyzed to a file 
     */
    public List<Project> writenewDocsToFile(String filename) {
    	int c = 0; 
    	List<Project> p = new ArrayList<Project>(); 
    	try {
    		   Directory dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));   
    		   IndexReader reader = DirectoryReader.open(dir);
    		   IndexSearcher searcher = new IndexSearcher(reader);   		

    	        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
    	        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
    	        iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);  
    		   
    		   Query q = new MatchAllDocsQuery();      		   
    		   Sort sort = new Sort(new SortedNumericSortField("isimscore", Type.INT, false));
    		   TopDocs topDocs = searcher.search(q, 100000, sort);
    		   File file = new File("fullIndexlucene-"+indexDirectory.getName()+filename+".json");
   			   FileWriter fileWriter = new FileWriter(file);

  			   boolean flag = false; 
    		   for (ScoreDoc scoreDoc : topDocs.scoreDocs) {  			  
    		      Document doc = searcher.doc(scoreDoc.doc);
	      
    		      //write row of column labels once
     		      if (!flag) {
      		    	  for (IndexableField field : doc.getFields() ) {
     		    	  fileWriter.write( field.name()+";");       		      
      		      flag = true; }     		    	  
      		      fileWriter.write("\n");
    		      p.add(new KickstarterProject(doc)); 
    		      for (IndexableField field : doc.getFields() ) {
    		    	  fileWriter.write(field.stringValue()+field.numericValue());
    		    	  fileWriter.write(";"); 
    		      }
    		      }
    		    }
    		   System.out.println("size: "+ c);  		   
      			fileWriter.flush();
       			fileWriter.close();
    		  
    		   reader.close(); 
    			} catch (IOException e) {System.out.println("error: "+e.getMessage());}
    		return p; 
    		}
    
    
    /**
     * read projects from json file using object mapper and store in index
     * file must be a json file conform to the one of the crowdfunding project classes to be indexed
     */
    public List<Project> readProjectsFromFile(File file, Class<? extends Project> cl) {
    	List<Project> list = new ArrayList<Project>();
    	ObjectMapper mapper = new ObjectMapper();
    	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
        	// Open the file
        	FileInputStream fstream = new FileInputStream(file);
        	BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        	String strLine;
        	//Read File Line By Line
        	while (( (strLine = br.readLine()) != null ))   {
        		JsonNode jsonNode = mapper.readTree(strLine);
        		JsonNode idNode = jsonNode.path("data"); 
        		Object o =  mapper.readValue(idNode.toString(), cl); 
        		list.add(cl.cast(o));
        	}
        	//Close the input stream
        	br.close(); 
        } catch (IOException e) {System.err.println(e.getMessage());} 
        return list; 
    }
 

}



