package at.draschkowitz.cfduktiction.indexer;

/**
 * Licensed to DigitalPebble Ltd under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * DigitalPebble licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import static com.digitalpebble.stormcrawler.Constants.StatusStreamName;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import com.digitalpebble.stormcrawler.Metadata;
import com.digitalpebble.stormcrawler.indexing.AbstractIndexerBolt;
import com.digitalpebble.stormcrawler.persistence.Status;

import at.draschkowitz.cfduktiction.indexer.NewsIndexer;
import at.draschkowitz.cfduktiction.model.NewsIndexerObject;

/**
 * StormCrawler Indexer which generates fields for indexing and sends them to the NewsIndexer
 */

@SuppressWarnings("serial")
public class MyStdOutIndexer extends AbstractIndexerBolt {
    OutputCollector _collector;
    NewsIndexer in; 
    
    public MyStdOutIndexer() {
    }
	
    
    @SuppressWarnings("rawtypes")
    @Override
    public void prepare(Map conf, TopologyContext context,
            OutputCollector collector) {
        super.prepare(conf, context, collector);
        _collector = collector;
    	in = NewsIndexer.getInstance(); 
    }

    @Override
    public void execute(Tuple tuple) {
    	System.out.println("EXECUTION");
        String url = tuple.getStringByField("url");

        // Distinguish the value used for indexing
        // from the one used for the status
        String normalisedurl = valueForURL(tuple);

        Metadata metadata = (Metadata) tuple.getValueByField("metadata");

        // should this document be kept?
        boolean keep = filterDocument(metadata);
        if (!keep) {
            // treat it as successfully processed even if
            // we do not index it
            _collector.emit(StatusStreamName, tuple, new Values(url, metadata,
                    Status.FETCHED));
            _collector.ack(tuple);
            return;
        }

        // display text of the document?
        if (fieldNameForText() != null) {
            String text = tuple.getStringByField("text");
            System.out.println("TEXT: "+fieldNameForText() + "\t" + text);
            
        }

        if (fieldNameForURL() != null) {
            System.out.println("URL: "+fieldNameForURL() + "\t"
                    + trimValue(normalisedurl));
        }

        // which metadata to display?
        NewsIndexerObject n = new NewsIndexerObject(metadata); //also use text here
        List<NewsIndexerObject> a = new ArrayList<NewsIndexerObject>();
        a.add(n); 
		in.index(a);

        for (String fieldName : metadata.keySet()){ 
            String[] values = metadata.getValues(fieldName);
            if (values != null) {
            }
        }

        _collector.emit(StatusStreamName, tuple, new Values(url, metadata,
                Status.FETCHED));
        _collector.ack(tuple);
    }
    
    private String trimValue(String a) {
    	return a; 
    }

}