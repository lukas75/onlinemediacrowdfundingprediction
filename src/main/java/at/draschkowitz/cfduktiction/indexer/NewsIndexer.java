	package at.draschkowitz.cfduktiction.indexer;

	import java.io.*;
	import java.nio.file.Paths;
	import java.util.List;

	import org.apache.lucene.analysis.Analyzer;
	import org.apache.lucene.analysis.standard.StandardAnalyzer;
	import org.apache.lucene.document.Document;
	import org.apache.lucene.index.CorruptIndexException;
	import org.apache.lucene.index.IndexWriter;
	import org.apache.lucene.index.IndexWriterConfig;
	import org.apache.lucene.index.Term;
	import org.apache.lucene.store.Directory;
	import org.apache.lucene.store.FSDirectory;
	import org.apache.lucene.store.LockObtainFailedException;

	import at.draschkowitz.cfduktiction.model.IndexingObject;


	/**
	 * Class to index News sites
	 * 
	 *  singleton class but not multi-thread ready, can't be subclassed as constructor is private
	 *  
	 *  parts from http://computergodzilla.blogspot.co.at/2015/01/calculate-cosine-similarity-using.html
	 */
	public class NewsIndexer {

		private static NewsIndexer instance = null; 
		//private String root = "/Users/lukasdraschkowitz/git/cf/Spring4MVCAngularJSExample/"; 
	    String INDEX_DIRECTORY = "lucene/news"; 
	    
	    
	    private NewsIndexer() {
	    }
		
		public static NewsIndexer getInstance() {
			   if(instance == null) {
			         instance = new NewsIndexer();
			      }
			   return instance;
		}


	    /**
	     * Method to index files 
	     * uses update document so there are no duplicate urls in the index
	     * also uses create or append mode so appends to index in case one already exists otherwise creates a new one
	     */
	    public synchronized void index(List<? extends IndexingObject> o) {
	    		if (o !=null) {
	    		Directory dir;
				try {
					dir = FSDirectory.open(Paths.get(INDEX_DIRECTORY));
		        Analyzer analyzer = new StandardAnalyzer(StandardAnalyzer.STOP_WORDS_SET);  // using stop words
		        IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
		            iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);

		        IndexWriter writer = new IndexWriter(dir, iwc);
	        for (IndexingObject f : o) {
	        	if (f!= null) {
	            Document doc = f.getDocument(); 
	            writer.updateDocument(new Term("url", doc.get("url")) , doc);
	        	}
	        }
	        writer.commit(); 
	        writer.close();
	    		}
				// CorruptIndexException, LockObtainFailedException, IOException
	    		catch(Exception e){System.out.println(e.getMessage());}
	        
	    }
	    
	    }

	}




