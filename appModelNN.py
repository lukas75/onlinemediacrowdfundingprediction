
#model and 

import tensorflow
import ujson as json
import requests
import pandas as pd
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.preprocessing import StandardScaler
import warnings
warnings.filterwarnings('ignore')
import keras
from keras.layers import Embedding
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras import optimizers
import numpy as np
from keras.utils import to_categorical
import numpy as np
import time

#first train the mdoel

#get training data 
url = 'http://localhost:8080/Spring4MVCAngularJSExample/train/'
r = requests.get(url)
data  = json.loads(r.content)


#create a dataframe and remove features not used for model creation 
df = pd.DataFrame(data)
ignore_features = ['pledged', 'blurb', 'conceptname', 'name', 'slug', 'launched_at', 'updated_at', 'state','staff_pick', 'id','pid','platform']
#ignore_features = ['pledged', 'blurb', 'conceptname', 'name', 'slug', 'launched_at', 'updated_at', 'state','staff_pick', 'id','pid','platform', 'newsstat', 'twitterstat', 'score', 'popscore', 'simscore']
features = list(df.columns)
features = [i for i in features if i not in ignore_features] 
X = df[features].values

#Encoding the categorical features(the independent variables)
labelencoder_X_1 = LabelEncoder()
X[:,2] = labelencoder_X_1.fit_transform(X[:,2])
labelencoder_X_2 = LabelEncoder()
X[:,3] = labelencoder_X_2.fit_transform(X[:,3])

onehotencoder = OneHotEncoder(categorical_features = [2,3])  #,3 1,2 funkt besser ! 
X = onehotencoder.fit_transform(X).toarray()

#create class label values from pledged amount and goal of projects 
df['label'] = np.where(df['goal']/2 < df['pledged'] , 1, np.where(df['goal']/2 >= df['pledged'], 0, 0))
label = "final_status"
y = df['label'].values

#feature scaling - fit and transform
sc = StandardScaler()
X_train = sc.fit_transform(X)

#Initaializing the ANN
classifier = Sequential()
#classifier.add(Embedding(X_train.shape[1], X_train.shape[1]//2), input_dim=X_train.shape[1])
#classifier.add(Flatten())
#Adding the input layer and the first hidden layer with dropout
#classifier.add(Dense(activation="relu", kernel_initializer="uniform", units=X_train.shape[1]//2))
classifier.add(Dense(activation="relu", kernel_initializer="uniform", input_dim=X_train.shape[1], units=X_train.shape[1]//2))
classifier.add(Dropout(rate = 0.1))
#Adding the second hidden layer
classifier.add(Dense(activation="relu", kernel_initializer="uniform", units=X_train.shape[1]//2))
classifier.add(Dropout(rate = 0.1))
#Adding the third hidden layer
#classifier.add(Dense(activation="relu", kernel_initializer="uniform", units=6))
#classifier.add(Dropout(rate = 0.1))
#Adding the output layer
classifier.add(Dense(activation="sigmoid", kernel_initializer="uniform", units=1))
#Compiling the ANN
#sgd = optimizers.SGD(lr=0.1, decay=1e-6, momentum=0.9, nesterov=True)
classifier.compile(optimizer = 'adam', loss = "binary_crossentropy", metrics=["accuracy"])

#Fitting the ANN to training set
classifier.fit(X, y, batch_size=10, epochs=50)


#wait for projects to be scored and get score from the trained model
url = 'http://localhost:8080/Spring4MVCAngularJSExample/toscore/'
while True: 
        #204 status code no content
        r = requests.get(url)
        while (r.status_code == 204): 
            time.sleep(.1000) # 1 seconds sleep
            r = requests.get(url)
        if (r.status_code != 204): 
            datar  = json.loads(r.content)
            print('got content')
            #feed data into modell and return object with score
            dfr = pd.DataFrame(datar)
            print(dfr)
            #compute scores
            ignore_features = ['pledged', 'blurb', 'conceptname', 'name', 'slug', 'launched_at', 'updated_at', 'state','staff_pick', 'id','pid','platform', 'video', 'label', 'score']
            for index, row in dfr.iterrows():
                #print(row['name'])
                features = list(df.columns)
                features = [i for i in features if i not in ignore_features] 
                X = row[features].values     
                X[2] = labelencoder_X_1.transform([X[2]])
                X[3] = labelencoder_X_2.transform([X[3]])
                #print(X)
                #print(X.reshape(1, -1))#.toarray())
                X = onehotencoder.transform(X.reshape(1, -1))
                #print(X.toarray())
                X = sc.transform(X.toarray())
                s = classifier.predict(X) 
                dfr.loc[[index],'score'] = s*10000
    
        #send back as post request 
        print('sending back')
        print(a)
        a = dfr.to_dict('records')
        headers = {'Content-Type': 'application/json', 'Accept':'application/json'}
        #By using the json keyword argument the data is encoded to JSON for you, and the Content-Type header is set to application/json.
        purl = 'http://localhost:8080/Spring4MVCAngularJSExample/scored/'
        requests.post(purl, json=a, headers=headers)



